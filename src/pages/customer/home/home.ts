import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, MenuController } from 'ionic-angular';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../services/constant';
import { CallNumber } from '@ionic-native/call-number';
import { TrackLoad } from '../trackLoad/trackLoad';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {


    loadListArry = [];
    currentPagination = 1;
    infiniteScrollEnable = true;
    indexExpandView = 10000;


    constructor(public menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public callNumber: CallNumber, public app: App) {

            this.menu.swipeEnable(false);

    }

    ionViewWillEnter() {
        this.loadListArry = [];
        this.OrderListAPI();
    }


    OrderListAPI() {
        var dict = {};
        dict["page"] = 1;

        this.constant.LoadingPresent();
        this.Service.OrderList(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.loadListArry.push(tempData[index]);
                }
                console.log('loadListArry=', this.loadListArry);
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    InfinitScrollingAPI(infiniteScroll: any) {


        var dic = {};
        this.currentPagination = this.currentPagination + 1
        dic['page'] = this.currentPagination;

        this.Service.OrderList(dic).subscribe((result) => {
            infiniteScroll.complete();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.loadListArry.push(tempData[index]);
                }
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
            if (result.data.has_more_pages) {
                this.infiniteScrollEnable = true;
            } else {
                this.infiniteScrollEnable = false;
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    expandViewClick(indexss) {
        if (this.indexExpandView == indexss) {
            this.indexExpandView = 10000;
        } else {
            this.indexExpandView = indexss;
        }

    }

    TrackOrderClick(item) {
        console.log('TrackOrderClick');
        this.app.getRootNav().push(TrackLoad, { orderID: item.id });
    }

    CallToDriverClick(item) {
        console.log(item);
        this.callNumber.callNumber(item.driver_phone, true).then(res => {
            console.log('Launched dialer!', res)
        }).catch(err => {
            console.log('Error launching dialer', err)
        });
    }



}
