import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, MenuController, App, Platform } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';
import { TranslateService } from '@ngx-translate/core';
import { WebService } from '../../../services/webService';
import { Constant } from '../../../services/constant';
import { Jsonp } from '@angular/http';
import { LoginPage } from '../../login/login/login';
import { ANDROID_APP_VERSION, IOS_APP_VERSION } from '../../../services/config';

@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html'
})
export class ProfilePage {  

    
    userData = [];
    appVersion = '';

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public app :App, public platform:Platform) {

        this.userData = this.constant.getUserData();
        console.log(this.userData);
    }

    ionViewWillEnter(){
        
        if (this.platform.is('android')) {
            this.appVersion = ANDROID_APP_VERSION;
        } else {
            this.appVersion = IOS_APP_VERSION;
        }

    }

    LogoutClick(){
        localStorage.clear();
        this.app.getRootNav().setRoot(LoginPage);
    }
    

}
