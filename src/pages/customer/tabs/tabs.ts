import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { Events, NavController, App } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';
import { SearchPage } from '../searchPage/searchPage';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class CustomerTab {

  tab1Root = SearchPage;
  tab2Root = HomePage;
  tab3Root = NotificationPage;
  tab4Root = ProfilePage;
  

  constructor(public events:Events, public nav: NavController, public app:App) {

   

  }


}
