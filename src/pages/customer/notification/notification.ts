import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, ViewController, App } from 'ionic-angular';
import { Constant } from '../../../services/constant';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { TrackLoad } from '../trackLoad/trackLoad';

@Component({
    selector: 'page-notification',
    templateUrl: 'notification.html'
})
export class NotificationPage {

    notificationArry = [];
    currentPagination = 1;
    infiniteScrollEnable = true;


    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public viewCtrl:ViewController, public app:App) {

          
    }  

    ionViewWillEnter(){
        this.notificationArry = [];
        this.notificationListAPI();
    }

    notificationListAPI() {
        var dict = {};
        dict["page"] = 1;
        
        this.constant.LoadingPresent();
        this.Service.NotificationList(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.notificationArry.push(tempData[index]);    
                }
                
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    InfinitScrollingAPI(infiniteScroll: any) {

        var dic = {};
        this.currentPagination = this.currentPagination + 1
        dic['page'] = this.currentPagination;

        this.Service.NotificationList(dic).subscribe((result) => {
            infiniteScroll.complete();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.notificationArry.push(tempData[index]);    
                }
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
            if (result.data.has_more_pages) {
                this.infiniteScrollEnable = true;
            } else {
                this.infiniteScrollEnable = false;
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    notificationClick(item){
        this.app.getRootNav().push(TrackLoad, { orderID: item.order_id });
    }
    
}
