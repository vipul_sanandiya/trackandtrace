import { Component, ChangeDetectorRef } from '@angular/core';
import { App, NavController, MenuController, ModalController, Events, Platform, AlertController } from 'ionic-angular';
import { Constant } from "../../../services/constant";
import { WebService } from "../../../services/webService";
import { TranslateService } from '@ngx-translate/core';
import { DriverTab } from '../tabs/tabs';
import { LoadDetailPage } from '../loadDetail/loadDetail';
import { LocationTracker } from '../../../services/locationTracker';
import { Geolocation } from '@ionic-native/geolocation';
import { ANDROID_APP_VERSION, IOS_APP_VERSION } from '../../../services/config';

@Component({
    templateUrl: 'myLoad.html',
    selector: 'page-myLoad'
})
export class MyLoadPage {

    activeStatus = 2;
    currentPagination = 1;
    infiniteScrollEnable = true;
    orderListArry = [];
    assignOrderCount = 0;
    notificationOrder = [];


    constructor(public menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public app: App, public locationTracker: LocationTracker, public geolocation: Geolocation, public platform: Platform, public alertCtrl: AlertController) {

            this.menu.swipeEnable(false);

    }

    ionViewWillEnter() {
        this.orderListArry = [];
        this.currentPagination = 1;
        this.locationTracker.stopTracking();
        this.activeStatus = 2;
        this.OrderListAPI(2);
    }

    OrderListAPI(loadStatus) {
        var dict = {};
        dict["page"] = 1;
        dict["order_status"] = loadStatus;

        this.constant.LoadingPresent();
        this.Service.OrderList(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.orderListArry = result.data.records;
                this.assignOrderCount = result.total_assigned_count;
                this.checkBackroundLocation();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    checkBackroundLocation() {
        this.locationTracker.stopTracking();
        for (let index = 0; index < this.orderListArry.length; index++) {
            // If current status is not accepted then location update
            if (this.orderListArry[index].status_data.current_status.id != 2 && this.orderListArry[index].status_data.current_status.id != 1) {
                if (this.platform.is('ios')) {
                    this.locationTracker.startTrackingWorkingIOS(this.orderListArry[index].id);
                } else {
                    this.locationTracker.startTrackingWorkingAndroid(this.orderListArry[index].id);
                }
                return;
            }
        }
    }

    InfinitScrollingAPI(infiniteScroll: any) {
        var dic = {};
        this.currentPagination = this.currentPagination + 1
        dic['page'] = this.currentPagination;
        dic["order_status"] = this.activeStatus;

        this.Service.OrderList(dic).subscribe((result) => {
            infiniteScroll.complete();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                this.assignOrderCount = result.total_assigned_count;
                for (let index = 0; index < tempData.length; index++) {
                    this.orderListArry.push(tempData[index]);
                }
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
            if (result.data.has_more_pages) {
                this.infiniteScrollEnable = true;
            } else {
                this.infiniteScrollEnable = false;
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    statusClick(statusActive) {
        this.activeStatus = statusActive;
        this.orderListArry = [];
        this.currentPagination = 1;
        this.infiniteScrollEnable = true;
        this.locationTracker.stopTracking();

        this.OrderListAPI(statusActive);
    }

    updateStatusClick(item, type){
        this.getUserLocation(item, type);
    }

    getUserLocation(item, type){
        this.constant.LoadingPresent();
        var onlyFirstTimeCall = true;
        this.geolocation.getCurrentPosition().then((resp) => {
            if (onlyFirstTimeCall) {
                onlyFirstTimeCall = false;
                this.constant.LoadingPresent();
                this.changeStatusAPI(item, type, resp.coords.latitude, resp.coords.longitude);    
            }
            console.log(resp);

        }).catch((error) => {
            this.constant.LoadingHide();
            console.log('Error getting location', error);
        });
    }

    changeStatusAPI(item, type, lat, lng) {
        var dict = {};
        dict["order_id"] = item.id;
        dict["order_action"] = type;
        dict["latitude"] = lat;
        dict["longitude"] = lng;

        if (this.platform.is('android')) {
            dict["version"] = ANDROID_APP_VERSION;
        } else {
            dict["version"] = IOS_APP_VERSION;
        }

        this.constant.LoadingPresent();
        this.Service.changeLoadStatus(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.orderListArry = [];
                this.currentPagination = 1;
                this.OrderListAPI(this.activeStatus);
            }
            this.constant.ToastCustom(result.message, 'bottom');

        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    DetailsClick(item) {
        console.log(item);
        this.app.getRootNav().push(LoadDetailPage, { orderID: item.id });
    }
}



// 1 - complete

// 2 - Active

// 3 - assigned