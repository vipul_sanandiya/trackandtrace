import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, MenuController, ViewController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { WebService } from '../../../services/webService';
import { Constant } from '../../../services/constant';
import { Geolocation } from '@ionic-native/geolocation';
import { _ParseAST } from '@angular/compiler';



@Component({
    selector: 'page-placeCategory',
    templateUrl: 'placeCategory.html'
})
export class PlaceCategory {

    

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public viewCtrl:ViewController) {


    }

    ionViewDidLoad() {
        
    }

    SelectCategory(items){
        this.viewCtrl.dismiss(items);
    }

}
