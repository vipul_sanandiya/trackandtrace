import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, MenuController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { WebService } from '../../../services/webService';
import { Constant } from '../../../services/constant';
import { Geolocation } from '@ionic-native/geolocation';
import { _ParseAST } from '@angular/compiler';
import { PlaceCategory } from '../placeCategory/placeCategory';

declare var google: any;
// Google search find place => https://developers.google.com/maps/documentation/javascript/places

@Component({
    selector: 'page-place',
    templateUrl: 'place.html'
})
export class PlacePage {

    @ViewChild('map') mapElement: ElementRef;
    map: any;
    userCurrentlat: any;
    userCurrentLong: any;
    searchCategoryType = ''
    options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };
    infoWindowArry = [];

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public geolocation: Geolocation) {


    }

    ionViewDidLoad() {
        this.getCurrentLocation();
    }

    getCurrentLocation() {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.userCurrentlat = resp.coords.latitude;
            this.userCurrentLong = resp.coords.longitude;
            console.log(resp);
            this.initMapTwo(false);
        }).catch((error) => {
            console.log('Error getting location', error);
        });

    }

    initMapTwo(values) {
        navigator.geolocation.getCurrentPosition((location) => {
            console.log(location);
            this.map = new google.maps.Map(this.mapElement.nativeElement, {
                center: { lat: location.coords.latitude, lng: location.coords.longitude },
                zoom: 10
            });
            if (values) {
                this.SearchOnGoogleMap();
            }
        }, (error) => {
            console.log(error);
        }, this.options);

    }

    SearchOnGoogleMap() {
        var service = new google.maps.places.PlacesService(this.map);

        service.textSearch({
            location: { lat: this.userCurrentlat, lng: this.userCurrentLong },
            radius: '100000',
            query: this.searchCategoryType
        }, (results, status) => {
            console.log(results);
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                for (var indez = 0; indez < results.length; indez++) {
                    this.createMarker(results[indez], indez);
                }
            }
        });

    }

    createMarker(place, indez) {
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();

        var placeLoc = { lat: latitude, lng: longitude }
        console.log(placeLoc);
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();

        var marker = new google.maps.Marker({
            map: this.map,
            title: place.name,
            position: placeLoc,
            index: indez
        });

        var self = this;

        google.maps.event.addListener(marker, 'click', function () {
            console.log(marker);
            self.infowindowManage(place.name, marker);
        });




    }


    infowindowManage(placename, marker) {
        for (let index = 0; index < this.infoWindowArry.length; index++) {
            var infotempClose = this.infoWindowArry[index];
            infotempClose.close();
        }
        var nfowindowTemp = new google.maps.InfoWindow({ map: this.map });
        nfowindowTemp.setContent(placename);
        
        nfowindowTemp.open(this.map, marker);
        this.infoWindowArry.push(nfowindowTemp);
    }

    MoreClick() {
        var modelContrl = this.modalCtrl.create(PlaceCategory);
        modelContrl.present();

        modelContrl.onDidDismiss(data => {
            console.log(data);
            this.searchCategoryType = data;
            this.initMapTwo(true);
        });
    }

    ClickCategory(categoryType) {
        this.searchCategoryType = categoryType;
        this.initMapTwo(true);
    }


}
