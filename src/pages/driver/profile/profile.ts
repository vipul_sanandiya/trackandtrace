import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, MenuController, App, Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { WebService } from '../../../services/webService';
import { Constant } from '../../../services/constant';
import { Jsonp } from '@angular/http';
import { LoginPage } from '../../login/login/login';
import { ResetPassword } from '../../login/resetPassword/resetPassword';
import { ANDROID_APP_VERSION, IOS_APP_VERSION } from '../../../services/config';

@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html'
})
export class DriverProfile {

    isEmailEditable = false;
    userData = [];
    emailModal:any = '';
    appVersion = '';

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public app: App, public platform:Platform) {

        this.userData = this.constant.getUserData();
        console.log(this.userData);
    }

    ionViewWillEnter() {
        if (this.platform.is('android')) {
            this.appVersion = ANDROID_APP_VERSION;
        } else {
            this.appVersion = IOS_APP_VERSION;
        }
    }

    emailEditClick() {
        if (this.isEmailEditable) {
            this.isEmailEditable = false;
        } else {
            this.isEmailEditable = true;
        }
    }

    saveClick(){
        if (!this.emailModal) {
            this.constant.ToastCustom('Please enter your email address.', 'bottom');
        } else if(!this.constant.isValidEmail(this.emailModal)){
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        }else{
            this.UpdateProfileAPI();
        }
    }
    
    UpdateProfileAPI() {
        var dict = {};
        dict["email"] = this.emailModal;
        
        this.constant.LoadingPresent();
        this.Service.UpdateProfile(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                localStorage.setItem('userData', JSON.stringify(result.data));
                this.userData = this.constant.getUserData();
                this.isEmailEditable = false
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    LogoutClick() {
        localStorage.clear();
        this.app.getRootNav().push(LoginPage);
    }


    ResetPasswordCLick(){
        this.navctrl.push(ResetPassword);
    }

}
