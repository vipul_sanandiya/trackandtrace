import { Component } from '@angular/core';
import { DriverProfile } from '../profile/profile';
import { Events, NavController, App } from 'ionic-angular';
import { MyLoadPage } from '../myLoad/myLoad';
import { PlacePage } from '../place/place';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class DriverTab {  

  tab1Root = PlacePage;
  tab2Root = MyLoadPage;
  // tab3Root = DriverNotification;
  tab4Root = DriverProfile;
  

  constructor(public events:Events, public nav: NavController, public app:App) {
    
   
  }


}
