import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, MenuController, ModalController, Events, NavParams } from 'ionic-angular';
import { Constant } from "../../../services/constant";
import { WebService } from "../../../services/webService";
import { TranslateService } from '@ngx-translate/core';
import { DriverTab } from '../tabs/tabs';

declare var google: any;

@Component({
    templateUrl: 'loadDetail.html',
    selector: 'page-loadDetail'
})
export class LoadDetailPage {

    activeTab = 1;
    orderListDetails = [];
    statusLogDetails = [];
    paramData: any;
    timeAndDistanceData = [];
    map: any;
    intervalTemp: any;
    marker: any;
    waypointArrayAllLocation = [];
    waypointArray = [];

    checkboxModel: any;
    markerArry = [];

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public navParam: NavParams) {

    }

    TabactiveClick(activetabis) {
        this.activeTab = activetabis;
        if (this.activeTab == 1) {
            this.checkboxModel = false;
            this.detectChange.detectChanges();
            this.initMap();
            this.detectChange.detectChanges();

        }

    }

    ionViewWillEnter() {
        this.paramData = this.navParam.get('orderID');
        this.OrderListAPI();
    }

    OrderListAPI() {
        var dict = {};
        dict["order_id"] = this.paramData;

        this.constant.LoadingPresent();
        this.Service.OrderDetails(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.orderListDetails = result.data.order_detail;
                this.statusLogDetails = result.data.status_log;
                this.waypointArrayAllLocation = result.data.location_data;
                this.initMap();
                this.checkForComingNotification();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    checkForComingNotification() {
        var isComingFromNotification = this.navParam.get('fromNotification');
        if (isComingFromNotification != null && isComingFromNotification != '' && isComingFromNotification != undefined) {
            this.readNotificationAPI();
        }
    }

    readNotificationAPI() {
        var dict = {};
        dict["order_id"] = this.paramData;
        dict["notification_type"] = this.navParam.get('fromNotification');

        this.constant.LoadingPresent();
        this.Service.readNotification(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {

            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    initMap() {
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();
        var latlng = new google.maps.LatLng(this.orderListDetails['pickup_latitude'], this.orderListDetails['pickup_longitude']);
        var mapOptions = {
            zoom: 7,
            center: latlng
        }
        this.map = new google.maps.Map(document.getElementById('mapContainers'), mapOptions);
        directionsDisplay.setMap(this.map);

        var sourceAddress = new google.maps.LatLng(this.orderListDetails['pickup_latitude'], this.orderListDetails['pickup_longitude']);
        var DestinationAddress = new google.maps.LatLng(this.orderListDetails['drop_latitude'], this.orderListDetails['drop_longitude']);

        for (let index = 0; index < this.waypointArrayAllLocation.length; index++) {
            console.log(this.waypointArrayAllLocation[index]);
            if (index != 0 && index != this.waypointArrayAllLocation.length - 1) {
                var point = new google.maps.LatLng(this.waypointArrayAllLocation[index].lat, this.waypointArrayAllLocation[index].lng);

                var dic = {};
                dic['location'] = point;
                dic['stopover'] = true;

                this.waypointArray.push(dic);
            }
        }

        var self = this;
        var request = {
            origin: sourceAddress,
            destination: DestinationAddress,
            waypoints: self.waypointArray,
            travelMode: 'DRIVING'
        };

        directionsService.route(request, function (result, status) {
            if (status == 'OK') {
                self.timeAndDistanceData = result.routes[0].legs[0];
                console.log(self.timeAndDistanceData);
                self.detectChange.detectChanges();
                directionsDisplay.setDirections(result);
            }
        });

        if (this.statusLogDetails[this.statusLogDetails.length - 1].status_id == 6) {
            this.timeCountForUpdateLocation();
            this.getTruckCurrentLocation();
        }


    }

    getTruckCurrentLocation() {
        var dict = {};
        dict["order_id"] = this.paramData;

        this.constant.LoadingPresent();
        this.Service.getTruckLocation(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                if (result.data.records) {
                    var lat = result.data.records.latitude;
                    var lng = result.data.records.longitude;
                    this.setMarker(lat, lng);
                }
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    setMarker(lat, lng) {
        console.log(lat, lng);
        var myLatLng = { lat: parseFloat(lat), lng: parseFloat(lng) };
        var image = new google.maps.MarkerImage('assets/imgs/truck.png', null, new google.maps.Point(0, 0), new google.maps.Point(50, 50));
        this.marker = new google.maps.Marker({
            position: myLatLng,
            map: this.map,
            icon: image,
        });

    }

    //---------------------------Live tracking of Truck Start----------------------------//


    timeCountForUpdateLocation() {
        clearInterval(this.intervalTemp);
        this.intervalTemp = setInterval(() => {
            console.log('----------------------Live tracking Start-------------------');
            this.setMarkerPositionAPI();
        }, 15000);
    }

    setMarkerPositionAPI() {
        var dict = {};
        dict["order_id"] = this.paramData;
        this.Service.getTruckLocation(dict).subscribe((result) => {
            console.log(result);
            if (result.status) {
                if (result.data.records) {
                    var lat = result.data.records.latitude;
                    var lng = result.data.records.longitude;
                    var latlng = new google.maps.LatLng(lat, lng);
                    this.marker.setPosition(latlng);
                    console.log('-----------------Marker new position set-----------------');
                }
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    ionViewWillLeave() {
        console.log('----------------------Live tracking Stop-------------------');
        clearInterval(this.intervalTemp);
    }

    //---------------------------Show all marker on Map----------------------------//

    checkBoxClick() {
        console.log(this.checkboxModel);
        if (this.checkboxModel) {
            this.getMarkerListAPI();
        }else{
            for (let index = 0; index < this.markerArry.length; index++) {
                this.markerArry[index].setMap(null);
            }
        }
    }

    getMarkerListAPI() {
        var dict = {};
        dict["order_id"] = this.paramData;

        this.constant.LoadingPresent();
        this.Service.getLoadsMapPointList(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.markerArry = [];

                for (let index = 0; index < result.data.length; index++) {
                    var tempData = result.data[index];
                    var myLatLng = { lat: parseFloat(tempData.latitude), lng: parseFloat(tempData.longitude) };
                    var markerTemp = new google.maps.Marker({
                        position: myLatLng,
                        map: this.map,
                        icon: 'assets/imgs/gps_marker.png',
                    });
                    this.markerArry.push(markerTemp);
                }


            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }
}