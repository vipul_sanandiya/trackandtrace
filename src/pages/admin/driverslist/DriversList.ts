import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App } from 'ionic-angular';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../services/constant';
import { CallNumber } from '@ionic-native/call-number';

import { AddUserPage } from '../AddUser/AddUser';
import { AddDriverPage } from '../AddDriver/AddDriver';

@Component({
    selector: 'page-DriversList',
    templateUrl: 'DriversList.html'
})
export class DriversListPage {

    // loadListArry = [];
    driverListArry = [];
    currentPagination = 1;
    infiniteScrollEnable = true;
    indexExpandView = 10000;
    searchModel: any;


    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public callNumber: CallNumber, public app: App, public alertCtrl:AlertController) {


    }

    ionViewWillEnter() {
        console.log('ionViewWillEnter');
        this.currentPagination = 1;
        this.infiniteScrollEnable = true;
        this.indexExpandView = 1000;
        this.driverListArry = [];
        this.DriverListAPI();
    }


    DriverListAPI() {
        this.currentPagination = 1;
        var dict = {};
        dict["page"] = this.currentPagination;
        this.constant.LoadingPresent();
        this.Service.DriverList(dict).subscribe((result) => {

            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.driverListArry.push(tempData[index]);
                }
                console.log('driverListArry=', this.driverListArry);
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    InfinitScrollingAPI(infiniteScroll: any) {

        console.log('InfinitScrollingAPI');
        var dic = {};
        this.currentPagination = this.currentPagination + 1
        dic['page'] = this.currentPagination;

        this.Service.DriverList(dic).subscribe((result) => {
            infiniteScroll.complete();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.driverListArry.push(tempData[index]);
                }
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
            if (result.data.has_more_pages) {
                this.infiniteScrollEnable = true;
            } else {
                this.infiniteScrollEnable = false;
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    searchChangeEvent() {
        var timer: any;
        clearTimeout(timer);
        console.log(this.searchModel);
        if (this.searchModel && this.searchModel != '' && this.searchModel != null) {
            timer = setTimeout(x => {
                this.searchLoadAPI();
            }, 1500);
        }
    }

    searchLoadAPI() {
        var dict = {};
        dict["search_text"] = this.searchModel;
        this.Service.DriverList(dict).subscribe((result) => {
            this.driverListArry = [];
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.driverListArry.push(tempData[index]);
                }
                console.log('driverListArry=', this.driverListArry);
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    onCancelSearch() {
        console.log('cancel search');
        this.driverListArry = [];
        this.DriverListAPI();
    }

    expandViewClick(indexss) {
        if (this.indexExpandView == indexss) {
            this.indexExpandView = 10000;
        } else {
            this.indexExpandView = indexss;
        }
    }

    deleteDriverClick(items){
        let alert = this.alertCtrl.create({
            title: 'Action Alert',
            message: 'Are you sure you want to delete this driver?',
            buttons: [
                {
                    text: 'Yes',
                    role: 'cancel',
                    handler: () => {
                        this.deleteDriverAPI(items);
                    }
                },
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alert.present();
    }

    deleteDriverAPI(driver) {
        console.log(driver);
        var dict = {};
        dict["id"] = driver.id;
        this.constant.LoadingPresent();
        this.Service.deleteDriver(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.ionViewWillEnter();
            }
            this.constant.ToastCustom(result.message, 'bottom');

        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    
    editDriverClick(driver) {
        console.log(driver);
        var modelContrl = this.modalCtrl.create(AddDriverPage, { driverID: driver });
        modelContrl.present();

        modelContrl.onDidDismiss(data => {
            console.log(data);
            this.ionViewWillEnter();
        });
    }

    AddNewdriverClick() {
        console.log('AddNewdriverClick');
        this.app.getRootNav().push(AddDriverPage);
    }




}
