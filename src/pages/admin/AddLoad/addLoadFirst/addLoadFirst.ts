import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, ViewController, NavParams } from 'ionic-angular';
import { WebService } from '../../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../../services/constant';
import { SelectCustomer } from '../../selectCustomer/selectCustomer';
import { AddLoadSecond } from '../addLoadSecond/addLoadSecond';

declare var google;

@Component({
    selector: 'page-addLoadAdmin',
    templateUrl: 'AddLoadFirst.html'
})
export class AddLoadFirst {


    firstData = {
        customerName: '',
        customerID: '',
        loadID: '',
        carrier: '',
        priority: 'high',
    }

    paramData: any;
    editOrderData: any;
    firstPageData:any;

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public app: App,
        public viewCtrl: ViewController, public navParam: NavParams) {



    }

    ionViewDidLoad() {
        this.paramData = this.navParam.get('loadID');
        console.log(this.paramData);
        if (this.paramData != undefined && this.paramData != '') {
            this.editLoadAPI();
        }
    }

    // -------------------------------Add Order-------------------------------//
    // -------------------------------Add Order-------------------------------//

    nextClick() {
        console.log(this.firstData.customerID);
        if (this.firstData.loadID == '' || this.firstData.loadID == undefined) {
            this.constant.ToastCustom('Please enter load.', 'top');
        } else if (this.firstData.priority == '' || this.firstData.priority == undefined) {
            this.constant.ToastCustom('Please select load priority.', 'top');
        } else {

            var dic = {};
            dic['edit_id'] = this.paramData;
            dic['form_step'] = 'first';
            dic['first'] = {
                customer_id: this.firstData.customerID,
                load_id: this.firstData.loadID,
                carrier: this.firstData.carrier,
                priority: this.firstData.priority,
            };
            this.addLoadAPIFirst(dic);
        }
        console.log(dic);
    }

    addLoadAPIFirst(dic) {

        this.constant.LoadingPresent();
        this.Service.addLoad(dic).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.detectChange.detectChanges();
                this.navctrl.push(AddLoadSecond, {first:dic.first, loadID:this.paramData});
            }
            this.constant.ToastCustom(result.message, 'bottom');
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }


    // -------------------------------Edit Order-------------------------------//
    // -------------------------------Edit Order-------------------------------//

    editLoadAPI() {
        var dict = {};
        dict["id"] = this.paramData;
        this.constant.LoadingPresent();
        this.Service.editLoad(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.editOrderData = result.data;
                this.firstPageData = result.data.first;
                this.editOrderOpration();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }

        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }


    editOrderOpration() {
        this.firstData.customerName = this.firstPageData.customer_id.customer_name;
        this.firstData.customerID = this.firstPageData.customer_id.id;
        this.firstData.loadID = this.firstPageData.load_id;
        this.firstData.carrier = this.firstPageData.carrier;
        this.firstData.priority = this.firstPageData.priority
    }

    firstCancelClick() {
        this.navctrl.pop();
    }

    customerNameSearchPageOepn() {
        console.log('customerNameSearchPageOepn');
        var modelContrl = this.modalCtrl.create(SelectCustomer, { type: "customer" });
        modelContrl.present();

        modelContrl.onDidDismiss(data => {
            console.log(data);
            if (data) {
                this.firstData.customerID = data.id;
                this.firstData.customerName = data.customer_name;
                this.detectChange.detectChanges();
            }
        });
    }

}
