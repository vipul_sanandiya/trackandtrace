import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, ViewController, NavParams } from 'ionic-angular';
import { WebService } from '../../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../../services/constant';
import { SelectCustomer } from '../../selectCustomer/selectCustomer';
import { LoadList } from '../../loadList/loadList';

declare var google;

@Component({
    selector: 'page-addLoadThird',
    templateUrl: 'addLoadThird.html'
})
export class AddLoadThird {

    currentFormAccess = 3;

    thirdDictionary = {};


    thirdData = {
        driverName: '',
        driverID: '',
        truckNumber: '',
        trailerNumber: '',
        deviceID: '',
        editID: ''
    }

    firstPageData:any;
    secondPageData:any;
    editOrderData: any;
    paramData:any;

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public app: App,
        public viewCtrl: ViewController, public navParam: NavParams) {



    }

    ionViewDidLoad() {
        this.paramData = this.navParam.get('loadID');
        console.log(this.paramData);
        this.firstPageData = this.navParam.get('first');
        console.log(this.firstPageData);
        this.secondPageData = this.navParam.get('second');
        console.log(this.secondPageData);

        if (this.paramData != undefined && this.paramData != '') {
            this.editLoadAPI();
        } else {
            
        }
    }

    // -------------------------------Edit Order-------------------------------//
    // -------------------------------Edit Order-------------------------------//

    editLoadAPI() {
        var dict = {};
        dict["id"] = this.paramData;
        this.constant.LoadingPresent();
        this.Service.editLoad(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.editOrderData = result.data;
                this.editOrderOpration(result.data.third);
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }

        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    editOrderOpration(data) {
        this.thirdData.driverName = data.driver_id.driver_name;
        this.thirdData.driverID = data.driver_id.id;
        this.thirdData.truckNumber = data.truck_number;
        this.thirdData.trailerNumber = data.trailer_number;
        this.thirdData.deviceID = data.device_id;
    }


    driverSearch(type) {
        var modelContrl = this.modalCtrl.create(SelectCustomer, { type: "driver" });
        modelContrl.present();

        modelContrl.onDidDismiss(data => {
            console.log(data);
            if (data) {
                this.thirdData.driverID = data.id;
                this.thirdData.driverName = data.driver_name;
            }
        });
    }

    saveClick() {
        this.thirdDictionary = {
            driver_id: this.thirdData.driverID,
            truck_number: this.thirdData.truckNumber,
            trailer_number: this.thirdData.trailerNumber,
            device_id: this.thirdData.deviceID,
        };
        this.addLoadThirdAPI();
    }

    addLoadThirdAPI() {
        
        var dic = {};
        dic['edit_id'] = this.paramData;
        dic['form_step'] = 'third';
        dic['first'] = this.firstPageData;
        dic['location_data'] = this.secondPageData;
        dic['third'] = this.thirdDictionary;
        console.log(dic);

        this.constant.LoadingPresent();
        this.Service.addLoad(dic).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.setRoot(LoadList);
            }
            this.constant.ToastCustom(result.message, 'bottom');
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    backClick() {
        this.currentFormAccess = this.currentFormAccess - 1;
    }

}
