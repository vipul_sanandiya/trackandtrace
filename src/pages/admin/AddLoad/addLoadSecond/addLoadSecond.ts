import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, ViewController, NavParams } from 'ionic-angular';
import { WebService } from '../../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../../services/constant';
import { SelectCustomer } from '../../selectCustomer/selectCustomer';
import { Jsonp } from '@angular/http';
import { _ParseAST } from '@angular/compiler';
import { AddLoadThird } from '../addLoadThird/addLoadThird';

declare var google;

@Component({
    selector: 'page-addLoadSecond',
    templateUrl: 'addLoadSecond.html'
})
export class AddLoadSecond {

    LocationDynamicArry = [];


    dropLocationDynamicDic = {
        "locationHeader": "Load Drop Point",
        "locationType": "load_drop_point",
        "mapDynamicID": "mapNumber",
        "locationID": "",
        "locationName": "",
        "latitude": "-34.9290",
        "longitude": "138.6010",
        "eLocation": "",
        "stopName": "",
        "refrence": "",
        "tracking": "",
        "eTime": "",
        "shippingFromHours": "",
        "shippingToHours": "",
        "appoinmentRequired": false,
        "instruction": ""
    }


    locationDynamicDic = {
        "locationHeader": "Load Pick-up Point",
        "locationType": "pick_up_point",
        "mapDynamicID": "mapNumber",
        "locationID": "",
        "locationName": "",
        "latitude": "-34.9290",
        "longitude": "138.6010",
        "eLocation": "",
        "stopName": "",
        "refrence": "",
        "tracking": "",
        "eTime": "",
        "shippingFromHours": "",
        "shippingToHours": "",
        "appoinmentRequired": false,
        "instruction": ""
    }

    firstPageData: any;
    paramData: any;
    editOrderData: any;

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public app: App,
        public viewCtrl: ViewController, public navParam: NavParams) {


    }

    ionViewDidLoad() {

        this.firstPageData = this.navParam.get('first');
        console.log(this.firstPageData);

        this.paramData = this.navParam.get('loadID');
        console.log(this.paramData);

        if (this.paramData != undefined && this.paramData != '') {
            this.editLoadAPI();
        } else {
            this.LocationDynamicArry.push(this.locationDynamicDic);
            this.LocationDynamicArry.push(this.dropLocationDynamicDic);

            this.detectChange.detectChanges();

            for (let index = 0; index < this.LocationDynamicArry.length; index++) {
                this.loadmap(this.LocationDynamicArry[index], index);
            }
        }

    }

    editLoadAPI() {
        var dict = {};
        dict["id"] = this.paramData;
        this.constant.LoadingPresent();
        this.Service.editLoad(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.editOrderData = result.data;
                this.editOrderOpration(result.data.location_data);
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }

        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    editOrderOpration(data) {


        for (let index = 0; index < data.length; index++) {

            if (data[index].record_type == "load_drop_point") {

                var dic = {};
                dic['locationHeader'] = 'Load Drop Point';
                dic['locationType'] = 'load_drop_point';
                dic['locationID'] = data[index].location_id.id;
                dic['locationName'] = data[index].location_id.location;
                dic['mapDynamicID'] = "mapNumber";
                dic['latitude'] = data[index].lat;
                dic['longitude'] = data[index].lng;
                dic['eLocation'] = data[index].external_location_id;
                dic['stopName'] = data[index].stop_name;
                dic['refrence'] = data[index].references;
                dic['tracking'] = data[index].tracking;

                var dateParseOne = new Date(data[index].earliest_appointment_time_timestamp * 1000);
                console.log(dateParseOne);
                dateParseOne.setMinutes(dateParseOne.getMinutes() - dateParseOne.getTimezoneOffset());
                dic['eTime'] = dateParseOne.toISOString();

                dic['shippingFromHours'] = data[index].from_shipping_hour;
                dic['shippingToHours'] = data[index].to_shipping_hour;
                dic['appoinmentRequired'] = this.apoinmentCheck(data[index].appointment_required);
                dic['instruction'] = data[index].instructions;

                this.LocationDynamicArry.push(dic);
            }

            if (data[index].record_type == "pick_up_point") {

                var dic = {};
                dic['locationHeader'] = 'Load Pick-up Point';
                dic['locationType'] = 'pick_up_point';
                dic['locationID'] = data[index].location_id.id;
                dic['locationName'] = data[index].location_id.location;
                dic['mapDynamicID'] = "mapNumber";
                dic['latitude'] = data[index].lat;
                dic['longitude'] = data[index].lng;
                dic['eLocation'] = data[index].external_location_id;
                dic['stopName'] = data[index].stop_name;
                dic['refrence'] = data[index].references;
                dic['tracking'] = data[index].tracking;


                var dateParseOne = new Date(data[index].earliest_appointment_time_timestamp * 1000);
                console.log(dateParseOne);
                dateParseOne.setMinutes(dateParseOne.getMinutes() - dateParseOne.getTimezoneOffset());
                dic['eTime'] = dateParseOne.toISOString();

                dic['shippingFromHours'] = data[index].from_shipping_hour;
                dic['shippingToHours'] = data[index].to_shipping_hour;
                dic['appoinmentRequired'] = this.apoinmentCheck(data[index].appointment_required);
                dic['instruction'] = data[index].instructions;

                this.LocationDynamicArry.push(dic);
            }
        }

        this.detectChange.detectChanges();

        for (let index = 0; index < this.LocationDynamicArry.length; index++) {
            this.loadmap(this.LocationDynamicArry[index], index);
        }

        this.detectChange.detectChanges();
    }

    apoinmentCheck(data) {

        if (data == 1) {
            return true;
        } else {
            return false;
        }

    }

    loadmap(locationItem, index) {
        var latlng = new google.maps.LatLng(locationItem.latitude, locationItem.longitude);
        var map = new google.maps.Map(document.getElementById(locationItem.mapDynamicID + index), {
            center: latlng,
            zoom: 12
        });

        if (locationItem.locationName) {
            let marker = new google.maps.Marker({ map: map, animation: google.maps.Animation.DROP, position: map.getCenter() });
        }
    }


    locationSearch(index) {
        var modelContrl = this.modalCtrl.create(SelectCustomer, { type: "location" });
        modelContrl.present();

        modelContrl.onDidDismiss(data => {
            console.log(data);
            if (data) {
                this.LocationDynamicArry[index].locationID = data.id;
                this.LocationDynamicArry[index].locationName = data.text;
                this.LocationDynamicArry[index].latitude = data.latitude;
                this.LocationDynamicArry[index].longitude = data.longitude;


                var latlng = new google.maps.LatLng(data.latitude, data.longitude);
                var map = new google.maps.Map(document.getElementById(this.LocationDynamicArry[index].mapDynamicID + index), {
                    center: latlng,
                    zoom: 12
                });

                let marker = new google.maps.Marker({ map: map, animation: google.maps.Animation.DROP, position: map.getCenter() });
            }
        });
    }

    // Add Remove Location

    addPickLocationAddClick() {
        var dic = {
            "locationHeader": "Load Pick-up Point",
            "locationType": "pick_up_point",
            "mapDynamicID": "mapNumber",
            "locationID": "",
            "locationName": "",
            "latitude": "-34.9290",
            "longitude": "138.6010",
            "eLocation": "",
            "stopName": "",
            "refrence": "",
            "tracking": "",
            "eTime": "",
            "shippingFromHours": "",
            "shippingToHours": "",
            "appoinmentRequired": false,
            "instruction": ""
        }
        this.LocationDynamicArry.splice((this.LocationDynamicArry.length - 1), 0, dic);
        this.detectChange.detectChanges();
        for (let index = 0; index < this.LocationDynamicArry.length; index++) {
            this.loadmap(this.LocationDynamicArry[index], index);
        }
    }


    addDropLocationAddClick() {
        var dic = {
            "locationHeader": "Load Drop Point",
            "locationType": "load_drop_point",
            "mapDynamicID": "mapNumber",
            "locationID": "",
            "locationName": "",
            "latitude": "-34.9290",
            "longitude": "138.6010",
            "eLocation": "",
            "stopName": "",
            "refrence": "",
            "tracking": "",
            "eTime": "",
            "shippingFromHours": "",
            "shippingToHours": "",
            "appoinmentRequired": false,
            "instruction": ""
        }
        this.LocationDynamicArry.splice((this.LocationDynamicArry.length - 1), 0, dic);
        this.detectChange.detectChanges();
        for (let index = 0; index < this.LocationDynamicArry.length; index++) {
            this.loadmap(this.LocationDynamicArry[index], index);
        }
    }

    nextClick() {
        console.log(this.LocationDynamicArry);
        console.log(JSON.stringify(this.LocationDynamicArry));

        var locationArry = [];
        for (let index = 0; index < this.LocationDynamicArry.length; index++) {
            var dic = {};
            dic['location_id'] = this.LocationDynamicArry[index].locationID;
            dic['external_location_id'] = this.LocationDynamicArry[index].eLocation;
            dic['stop_name'] = this.LocationDynamicArry[index].stopName;
            dic['references'] = this.LocationDynamicArry[index].refrence;
            dic['tracking'] = this.LocationDynamicArry[index].tracking;

            if (this.LocationDynamicArry[index].eTime) {
                var dateParseOne = new Date((this.LocationDynamicArry[index].eTime));
                dateParseOne.setMinutes(dateParseOne.getMinutes() + dateParseOne.getTimezoneOffset());
                var dateFormateTemp = this.constant.dateConvertFormate(dateParseOne, 'yyyy-MM-dd HH:mm:ss');
                dic['earliest_appointment_time'] = dateFormateTemp;
                dic['earliest_appointment_time_timestamp'] = dateParseOne.getTime();
            }


            dic['from_shipping_hour'] = this.LocationDynamicArry[index].shippingFromHours;
            dic['to_shipping_hour'] = this.LocationDynamicArry[index].shippingToHours;
            dic['appointment_required'] = this.LocationDynamicArry[index].appoinmentRequired;
            dic['instructions'] = this.LocationDynamicArry[index].instruction;
            dic['record_type'] = this.LocationDynamicArry[index].locationType;

            locationArry.push(dic);
        }

        this.addLoadAPISecond(locationArry)

    }

    addLoadAPISecond(locationArry) {
        var dict = {};
        dict["form_step"] = 'second';
        dict["location_data"] = locationArry;

        console.log('this.secondDictionary');
        this.constant.LoadingPresent();
        this.Service.addLoad(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.push(AddLoadThird, { first: this.firstPageData, second: locationArry, loadID: this.paramData });
            }
            this.constant.ToastCustom(result.message, 'bottom');
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    RemovePickLocationClick(index) {
        this.LocationDynamicArry.splice(index, 1);
    }

    backClick() {
        this.navctrl.pop();
    }



} 
