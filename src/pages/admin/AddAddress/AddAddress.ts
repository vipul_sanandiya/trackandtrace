import { Component, ChangeDetectorRef, NgZone, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, Platform, ViewController, NavParams } from 'ionic-angular';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../services/constant';
import { CallNumber } from '@ionic-native/call-number';

declare var google;

@Component({
    selector: 'page-AddAddress',
    templateUrl: 'AddAddress.html'
})
export class AddAddressPage {


    autocompleteItems;
    map: any;
    dataModel = {
        searchLocation: '',
        locationName: '',
        addressLineOne: '',
        addressLineTwo: '',
        city: '',
        state: '',
        country: '',
        postCode: '',
        latitude: '',
        longitude: '',
        addressID:''
    }

    paramData: any;

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public platform: Platform, public viewCtrl: ViewController, public navParam: NavParams) {

    }


    ionViewWillEnter() {
        this.paramData = this.navParam.get('AddressID');
        if (this.paramData != undefined && this.paramData != '') {
            this.getAddressDetailsAPI();
        }else{
            this.loadMap();
        }
    }

    getAddressDetailsAPI() {

        var dic = {};
        dic['id'] = this.paramData.id;
        console.log(dic);
        this.constant.LoadingPresent();
        this.Service.viewAddressDetails(dic).subscribe((result) => {
            console.log(result);
            if (result.status) {
                this.dataModel.searchLocation = result.data.location_text;
                this.dataModel.locationName = result.data.location_name;
                this.dataModel.addressLineOne = result.data.address_line_1;
                this.dataModel.addressLineTwo = result.data.address_line_2;
                this.dataModel.city = result.data.city;
                this.dataModel.state = result.data.state;
                this.dataModel.country = result.data.country;
                this.dataModel.postCode = result.data.postal_code;
                this.dataModel.latitude = result.data.latitude.toString();
                this.dataModel.longitude = result.data.longitude.toString();
                this.dataModel.addressID = result.data.id;

                this.loadMap();
                this.detectChange.detectChanges();
            }
            this.constant.LoadingHide();
            this.constant.ToastCustom(result.msg, 'bottom');
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    SearchLocationChange() {
        console.log('autocompleteFocus');
        var self = this;
        var options = {
            componentRestrictions: {
                //   country: 'ID' 
            }
        }
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('search_address'), options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            self.oprationONGoogleLocation(place);
        })
    }

    oprationONGoogleLocation(place) {
        console.log(place);

        this.dataModel.searchLocation = place.formatted_address;
        this.dataModel.latitude = place.geometry.location.lat().toString();
        this.dataModel.longitude = place.geometry.location.lng().toString();;

        var placeObject = place.address_components;
        for (let index = 0; index < placeObject.length; index++) {
            var arryObject = placeObject[index];
            if (arryObject.types[0] == 'country') {
                this.dataModel.country = arryObject.long_name;
            }

            if (arryObject.types[0] == 'administrative_area_level_1') {
                this.dataModel.state = arryObject.long_name;
            }

            // "colloquial_area", "locality", "political"
            if (arryObject.types[0] == 'administrative_area_level_2') {
                this.dataModel.city = arryObject.long_name;
            }

            if (arryObject.types[0] == 'postal_code') {
                this.dataModel.postCode = arryObject.long_name;
            }
        }

        this.loadMap();
        this.detectChange.detectChanges();
        console.log(this.dataModel);
    }

    loadMap() {
        var latlng = new google.maps.LatLng( parseFloat(this.dataModel.latitude), parseFloat( this.dataModel.longitude));
        this.map = new google.maps.Map(document.getElementById('mapContainers'), {
            center: latlng,
            zoom: 13,
        });

        var self = this;
        let marker = new google.maps.Marker({
            map: self.map,
            animation: google.maps.Animation.DROP,
            position: { lat: parseFloat(self.dataModel.latitude), lng: parseFloat(self.dataModel.longitude) },
        });
    }

    selectAddress(item) {
        console.log(item);
    }

    saveClick() {

        if (!this.dataModel.searchLocation) {
            this.constant.ToastCustom('Please search location', 'top');
        } else if (!this.dataModel.locationName) {
            this.constant.ToastCustom('Please enter location name', 'top');
        } else if (!this.dataModel.addressLineOne) {
            this.constant.ToastCustom('Please enter address line one ', 'top');
        } else if (!this.dataModel.city) {
            this.constant.ToastCustom('Please enter city name', 'top');
        } else if (!this.dataModel.state) {
            this.constant.ToastCustom('Please enter state name', 'top');
        } else if (!this.dataModel.postCode) {
            this.constant.ToastCustom('Please enter postal code', 'top');
        } else if (!this.dataModel.country) {
            this.constant.ToastCustom('Please enter country name', 'top');
        } else {
            console.log('done');
            if (this.dataModel.addressID) {
                this.updateAddressAPI();
            }else{
                this.AddAddressAPI();
            }
            
        }

    }

    AddAddressAPI() {

        var dic = {};
        dic['location_text'] = this.dataModel.searchLocation;
        dic['location_name'] = this.dataModel.locationName;
        dic['address_line_1'] = this.dataModel.addressLineOne;
        dic['address_line_2'] = this.dataModel.addressLineTwo;
        dic['city'] = this.dataModel.city;
        dic['state'] = this.dataModel.state;
        dic['postal_code'] = this.dataModel.postCode;
        dic['country'] = this.dataModel.country;
        dic['latitude'] = this.dataModel.latitude;
        dic['longitude'] = this.dataModel.longitude;
        
        console.log(dic);

        this.constant.LoadingPresent();
        this.Service.AddAddressManager(dic).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            }
            this.constant.ToastCustom(result.msg, 'bottom');
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    updateAddressAPI() {

        var dic = {};
        dic['location_text'] = this.dataModel.searchLocation;
        dic['location_name'] = this.dataModel.locationName;
        dic['address_line_1'] = this.dataModel.addressLineOne;
        dic['address_line_2'] = this.dataModel.addressLineTwo;
        dic['city'] = this.dataModel.city;
        dic['state'] = this.dataModel.state;
        dic['postal_code'] = this.dataModel.postCode;
        dic['country'] = this.dataModel.country;
        dic['latitude'] = this.dataModel.latitude;
        dic['longitude'] = this.dataModel.longitude;
        dic['id'] = this.dataModel.addressID;
        
        console.log(dic);

        this.constant.LoadingPresent();
        this.Service.updateAddressDetails(dic).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            }
            this.constant.ToastCustom(result.msg, 'bottom');
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    cancelClick(){
        this.navctrl.pop();
    }
}
