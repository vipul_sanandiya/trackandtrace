import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, ViewController, NavParams } from 'ionic-angular';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../services/constant';
import { CallNumber } from '@ionic-native/call-number';
import { ImageUpload } from '../../../helper/imageUpload/imageUpload';


@Component({
    selector: 'page-AddShipper',
    templateUrl: 'AddShipper.html'
})
export class AddShipperPage {

    shipperModel = {
        business_name: '',
        email: '',
        password: '',
        confirm_password: '',
        phone: '',
        extra_phone: '',
        address: '',
        address_details: '',
        profilePic: '',
        shipperID: ''
    };


    paramData: any;
    editShipperData: any;

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public callNumber: CallNumber, public app: App, public viewCtrl: ViewController, public navParam: NavParams) {


    }


    ionViewDidLoad() {
        this.paramData = this.navParam.get('shipperID');
        if (this.paramData != undefined && this.paramData != '') {
            this.editShipperAPI();
        }
    }

    editShipperAPI() {
        var dict = {};
        dict["id"] = this.paramData;
        this.constant.LoadingPresent();
        this.Service.editShipper(dict).subscribe((result) => {
            this.constant.LoadingHide();
            if (result.status) {
                this.editShipperData = result.data;
                this.editOperation();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    editOperation() {
        this.shipperModel.business_name = this.editShipperData.name;
        this.shipperModel.email = this.editShipperData.email;
        this.shipperModel.phone = this.editShipperData.phone;
        this.shipperModel.extra_phone = this.editShipperData.extra_phone;
        this.shipperModel.address = this.editShipperData.address;
        this.shipperModel.address_details = this.editShipperData.address_details;
        this.shipperModel.profilePic = this.editShipperData.image_url;
        this.shipperModel.shipperID = this.editShipperData.id;
    }

    uploadProfilePIC() {

        let imageUploadModel = this.modalCtrl.create(ImageUpload);
        imageUploadModel.present();
        imageUploadModel.onDidDismiss(data => {
            console.log(data);
            if (data) {
                this.shipperModel.profilePic = data;
            }
        });

    }

    saveClick() {
        if (this.shipperModel.shipperID) {
            this.editShipperValidation();
        } else {
            this.addShipperValidation();
        }
    }

    editShipperValidation() {
        if (!this.shipperModel.business_name) {
            this.constant.ToastCustom('Please enter business name.', 'bottom');
        } else if (!this.shipperModel.email) {
            this.constant.ToastCustom('Please enter email address.', 'bottom');
        } else if (!this.constant.isValidEmail(this.shipperModel.email)) {
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        } else if (!this.shipperModel.phone) {
            this.constant.ToastCustom('Please enter contact number.', 'bottom');
        } else {
            this.UpdateShipperAPI();
            console.log('Done');
        }
    }

    addShipperValidation() {
        if (!this.shipperModel.business_name) {
            this.constant.ToastCustom('Please enter business name.', 'bottom');
        } else if (!this.shipperModel.email) {
            this.constant.ToastCustom('Please enter email address.', 'bottom');
        } else if (!this.constant.isValidEmail(this.shipperModel.email)) {
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        } else if (!this.shipperModel.password) {
            this.constant.ToastCustom('Please enter password.', 'bottom');
        } else if ((this.shipperModel.password).length < 5) {
            this.constant.ToastCustom('Please enter minimum 6 digit password.', 'bottom');
        } else if (!this.shipperModel.confirm_password) {
            this.constant.ToastCustom('Please enter confirm password.', 'bottom');
        } else if (this.shipperModel.password != this.shipperModel.confirm_password) {
            this.constant.ToastCustom("Password and Confirm Password does't match.", 'bottom');
        } else if (!this.shipperModel.phone) {
            this.constant.ToastCustom('Please enter contact number.', 'bottom');
        } else {
            this.AddShipperAPI();
            console.log('Done');
        }
    }

    AddShipperAPI() {
        var dict = {};
        dict["name"] = this.shipperModel.business_name;
        dict["email"] = this.shipperModel.email;
        dict["password"] = this.shipperModel.password;
        dict["confirm_password"] = this.shipperModel.confirm_password;
        dict["phone"] = this.shipperModel.phone;
        dict["extra_phone"] = this.shipperModel.extra_phone;
        dict["address"] = this.shipperModel.address;
        dict["address_details"] = this.shipperModel.address_details;
        dict["image"] = this.shipperModel.profilePic;
        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.addShipper(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    UpdateShipperAPI() {
        var dict = {};
        dict["name"] = this.shipperModel.business_name;
        dict["email"] = this.shipperModel.email;
        dict["phone"] = this.shipperModel.phone;
        dict["extra_phone"] = this.shipperModel.extra_phone;
        dict["address"] = this.shipperModel.address;
        dict["address_details"] = this.shipperModel.address_details;

        if (!this.validURL(this.shipperModel.profilePic)) {
            dict["image"] = this.shipperModel.profilePic;
        }
        dict["id"] = this.shipperModel.shipperID;
        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.updateShipper(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    cancelClick() {
        this.navctrl.pop();
    }


    validURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    }


}
