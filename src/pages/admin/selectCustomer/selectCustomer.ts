import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, ViewController, NavParams } from 'ionic-angular';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../services/constant';
import { CallNumber } from '@ionic-native/call-number';
// import { TrackLoad } from '../trackLoad/trackLoad';


@Component({
    selector: 'page-selectCustomer',
    templateUrl: 'selectCustomer.html'
})
export class SelectCustomer {

    currentFormAccess = 1;

    searchModel:any;
    customerListArry = [];
    locationListArry = [];
    driverListArry = [];
    searchType = '';

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,public viewCtrl:ViewController,
    public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService,public app:App,
    public dataparam: NavParams) {

        this.searchType = this.dataparam.get('type');
        console.log(this.searchType);

    }

    ionViewWillEnter() {
        
    }

    CustomerNameSearch(){
        console.log(this.searchModel);
        if (this.searchModel && this.searchModel != '' && this.searchModel != null) {
            
            if (this.searchType == 'customer') {
                this.searchCustomerAPI();    
            }
            if (this.searchType == 'location') {
                this.searchLocationAPI();    
            }
            if (this.searchType == 'driver') {
                this.searchDriverAPI();    
            }
        }
    }

    

    searchCustomerAPI() {
        var dict = {};
        dict["search_text"] = this.searchModel;
        this.Service.searchCustomerList(dict).subscribe((result) => {
            console.log(result);
            if (result.status) {
                this.customerListArry = result.data;
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    searchLocationAPI() {
        var dict = {};
        dict["search_text"] = this.searchModel;
        this.Service.searchLocationList(dict).subscribe((result) => {
            console.log(result);
            if (result.status) {
                this.locationListArry = result.data;
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    searchDriverAPI() {
        var dict = {};
        dict["search_text"] = this.searchModel;
        this.Service.searchDriverList(dict).subscribe((result) => {
            console.log(result);
            if (result.status) {
                this.driverListArry = result.data;
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    customerSelectClick(items){
        this.viewCtrl.dismiss(items);
    }

    CloseClick(){
        this.viewCtrl.dismiss();
    }
    

   

}
