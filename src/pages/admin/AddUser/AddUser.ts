import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, ViewController, NavParams } from 'ionic-angular';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../services/constant';
import { CallNumber } from '@ionic-native/call-number';
import { ImageUpload } from '../../../helper/imageUpload/imageUpload';
// import { TrackLoad } from '../trackLoad/trackLoad';


@Component({
    selector: 'page-AddUser',
    templateUrl: 'AddUser.html'
})
export class AddUserPage {


    adminUserModel = {
        user_type_id: 0,
        firstName : '',
        lastName : '',
        email: '',
        password: '',
        confirm_password: '',
        phone: '',
        extra_phone: '',
        address: '',
        address_details: '',
        profilePic: '',
        adminUserID: ''
    };


    paramData: any;
    editAdminUserData: any;
    userAdminTypeList = [];

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public callNumber: CallNumber, public app: App, public viewCtrl: ViewController, public navParam: NavParams) {

        this.adminUserTypeLIstAPI();
    }


    ionViewDidLoad() {
        this.paramData = this.navParam.get('UserID');
        if (this.paramData != undefined && this.paramData != '') {
            this.editUserAPI();
        }
    }

    adminUserTypeLIstAPI() {
        var dict = {};
        this.constant.LoadingPresent();
        this.Service.adminUserTypeList(dict).subscribe((result) => {
            this.constant.LoadingHide();
            if (result.status) {
                this.userAdminTypeList = result.data;
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    editUserAPI() {
        var dict = {};
        dict["id"] = this.paramData;
        this.constant.LoadingPresent();
        this.Service.EditAdminuser(dict).subscribe((result) => {
            this.constant.LoadingHide();
            if (result.status) {
                this.editAdminUserData = result.data;
                this.editOperation();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    editOperation() {
        this.adminUserModel.user_type_id = this.editAdminUserData.user_type_id;
        this.adminUserModel.firstName = this.editAdminUserData.firstname;
        this.adminUserModel.lastName = this.editAdminUserData.lastname;  
        this.adminUserModel.email = this.editAdminUserData.email;
        this.adminUserModel.phone = this.editAdminUserData.phone;
        this.adminUserModel.extra_phone = this.editAdminUserData.extra_phone;
        this.adminUserModel.address = this.editAdminUserData.address;
        this.adminUserModel.address_details = this.editAdminUserData.address_details;
        this.adminUserModel.profilePic = this.editAdminUserData.image_url;
        this.adminUserModel.adminUserID = this.editAdminUserData.id;
    }

    uploadProfilePIC() {
        let imageUploadModel = this.modalCtrl.create(ImageUpload);
        imageUploadModel.present();
        imageUploadModel.onDidDismiss(data => {
            console.log(data);
            if (data) {
                this.adminUserModel.profilePic = data;
            }
        });
    }

    saveClick() {
        if (this.adminUserModel.adminUserID) {
            this.editAdminUserValidation();
        } else {
            this.addAdminUserValidation();
        }
    }

    editAdminUserValidation() {
        if (!this.adminUserModel.user_type_id) {
            this.constant.ToastCustom('Please select user type.', 'bottom');
        } else if (!this.adminUserModel.firstName) {
            this.constant.ToastCustom('Please enter first name.', 'bottom');
        } else if (!this.adminUserModel.lastName) {
            this.constant.ToastCustom('Please enter last name.', 'bottom');
        } else if (!this.adminUserModel.email) {
            this.constant.ToastCustom('Please enter email address.', 'bottom');
        } else if (!this.constant.isValidEmail(this.adminUserModel.email)) {
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        } else if (!this.adminUserModel.phone) {
            this.constant.ToastCustom('Please enter contact number.', 'bottom');
        } else {
            this.UpdateAdminUserAPI();
            console.log('Done');
        }
    }

    addAdminUserValidation() {
        if (!this.adminUserModel.user_type_id) {
            this.constant.ToastCustom('Please select user type.', 'bottom');
        } else if (!this.adminUserModel.firstName) {
            this.constant.ToastCustom('Please enter first name.', 'bottom');
        } else if (!this.adminUserModel.lastName) {
            this.constant.ToastCustom('Please enter last name.', 'bottom');
        } else if (!this.adminUserModel.email) {
            this.constant.ToastCustom('Please enter email address.', 'bottom');
        } else if (!this.constant.isValidEmail(this.adminUserModel.email)) {
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        } else if (!this.adminUserModel.password) {
            this.constant.ToastCustom('Please enter password.', 'bottom');
        } else if ((this.adminUserModel.password).length < 5) {
            this.constant.ToastCustom('Please enter minimum 6 digit password.', 'bottom');
        } else if (!this.adminUserModel.confirm_password) {
            this.constant.ToastCustom('Please enter confirm password.', 'bottom');
        } else if (this.adminUserModel.password != this.adminUserModel.confirm_password) {
            this.constant.ToastCustom("Password and Confirm Password does't match.", 'bottom');
        } else if (!this.adminUserModel.phone) {
            this.constant.ToastCustom('Please enter contact number.', 'bottom');
        } else {
            this.AddAdminUserAPI();
            console.log('Done');
        }
    }

    AddAdminUserAPI() {
        var dict = {};
        dict["user_type_id"] = this.adminUserModel.user_type_id;
        dict["firstname"] = this.adminUserModel.firstName;
        dict["lastname"] = this.adminUserModel.lastName;
        dict["email"] = this.adminUserModel.email;
        dict["password"] = this.adminUserModel.password;
        dict["confirm_password"] = this.adminUserModel.confirm_password;
        dict["phone"] = this.adminUserModel.phone;
        dict["extra_phone"] = this.adminUserModel.extra_phone;
        dict["address"] = this.adminUserModel.address;
        dict["address_details"] = this.adminUserModel.address_details;
        dict["image"] = this.adminUserModel.profilePic;
        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.AddAdminuser(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    UpdateAdminUserAPI() {
        var dict = {};
        dict["user_type_id"] = this.adminUserModel.user_type_id;
        dict["firstname"] = this.adminUserModel.firstName;
        dict["lastname"] = this.adminUserModel.lastName;
        dict["email"] = this.adminUserModel.email;
        dict["phone"] = this.adminUserModel.phone;
        dict["extra_phone"] = this.adminUserModel.extra_phone;
        dict["address"] = this.adminUserModel.address;
        dict["address_details"] = this.adminUserModel.address_details;
        
        if (!this.validURL(this.adminUserModel.profilePic)) {
            dict["image"] = this.adminUserModel.profilePic;
        }
        dict["id"] = this.adminUserModel.adminUserID;
        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.UpdateAdminuser(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    cancelClick() {
        this.navctrl.pop();
    }


    validURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    }



}
