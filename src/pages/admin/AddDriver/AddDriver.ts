import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App, ViewController, NavParams } from 'ionic-angular';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../services/constant';
import { CallNumber } from '@ionic-native/call-number';
import { DriversListPage } from '../driverslist/DriversList';
import { ImageUpload } from '../../../helper/imageUpload/imageUpload';
// import { TrackLoad } from '../trackLoad/trackLoad';


@Component({
    selector: 'page-AddDriver',
    templateUrl: 'AddDriver.html'
})
export class AddDriverPage {


    driverModel = {
        firstname: '',
        lastname: '',
        email: '',
        password: '',
        confirm_password: '',
        phone: '',
        extra_phone: '',
        licence_no: '',
        vehicle_no: '',
        address: '',
        address_details: '',
        profilePic: '',
        driverID: ''
    };


    paramData: any;
    editDriverData: any;

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public callNumber: CallNumber, public app: App, public viewCtrl: ViewController, public navParam: NavParams) {


    }


    ionViewDidLoad() {
        this.paramData = this.navParam.get('driverID');
        if (this.paramData != undefined && this.paramData != '') {
            this.editDriverAPI();
        }
    }

    editDriverAPI() {
        var dict = {};
        dict["id"] = this.paramData;
        this.constant.LoadingPresent();
        this.Service.editDriver(dict).subscribe((result) => {
            this.constant.LoadingHide();
            if (result.status) {
                this.editDriverData = result.data;
                this.editOperation();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    editOperation() {
        this.driverModel.firstname = this.editDriverData.firstname;
        this.driverModel.lastname = this.editDriverData.lastname;
        this.driverModel.email = this.editDriverData.email;
        this.driverModel.phone = this.editDriverData.phone;
        this.driverModel.extra_phone = this.editDriverData.extra_phone;
        this.driverModel.licence_no = this.editDriverData.licence_no;
        this.driverModel.vehicle_no = this.editDriverData.vehicle_no;
        this.driverModel.address = this.editDriverData.address;
        this.driverModel.address_details = this.editDriverData.address_details;
        this.driverModel.profilePic = this.editDriverData.image_url;
        this.driverModel.driverID = this.editDriverData.id;
    }

    uploadProfilePIC() {

        let imageUploadModel = this.modalCtrl.create(ImageUpload);
        imageUploadModel.present();
        imageUploadModel.onDidDismiss(data => {
            console.log(data);
            if (data) {
                this.driverModel.profilePic = data;
            }
        });

    }

    saveClick() {
        if (this.driverModel.driverID) {
            this.editDriverValidation();
        } else {
            this.addDriverValidation();
        }
    }

    editDriverValidation() {
        if (!this.driverModel.firstname) {
            this.constant.ToastCustom('Please enter first name.', 'bottom');
        } else if (!this.driverModel.lastname) {
            this.constant.ToastCustom('Please enter last name.', 'bottom');
        } else if (!this.driverModel.email) {
            this.constant.ToastCustom('Please enter email address.', 'bottom');
        } else if (!this.constant.isValidEmail(this.driverModel.email)) {
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        } else if (!this.driverModel.phone) {
            this.constant.ToastCustom('Please enter contact number.', 'bottom');
        } else {
            this.updateDriverAPI();
            console.log('Done');
        }
    }

    addDriverValidation() {
        if (!this.driverModel.firstname) {
            this.constant.ToastCustom('Please enter first name.', 'bottom');
        } else if (!this.driverModel.lastname) {
            this.constant.ToastCustom('Please enter last name.', 'bottom');
        } else if (!this.driverModel.email) {
            this.constant.ToastCustom('Please enter email address.', 'bottom');
        } else if (!this.constant.isValidEmail(this.driverModel.email)) {
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        } else if (!this.driverModel.password) {
            this.constant.ToastCustom('Please enter password.', 'bottom');
        } else if ((this.driverModel.password).length < 5) {
            this.constant.ToastCustom('Please enter minimum 6 digit password.', 'bottom');
        } else if (!this.driverModel.confirm_password) {
            this.constant.ToastCustom('Please enter confirm password.', 'bottom');
        } else if (this.driverModel.password != this.driverModel.confirm_password) {
            this.constant.ToastCustom("Password and Confirm Password does't match.", 'bottom');
        } else if (!this.driverModel.phone) {
            this.constant.ToastCustom('Please enter contact number.', 'bottom');
        } else {
            this.addDriverAPI();
            console.log('Done');
        }
    }

    addDriverAPI() {
        var dict = {};
        dict["firstname"] = this.driverModel.firstname;
        dict["lastname"] = this.driverModel.lastname;
        dict["email"] = this.driverModel.email;
        dict["password"] = this.driverModel.password;
        dict["confirm_password"] = this.driverModel.confirm_password;
        dict["phone"] = this.driverModel.phone;
        dict["extra_phone"] = this.driverModel.extra_phone;
        dict["licence_no"] = this.driverModel.licence_no;
        dict["vehicle_no"] = this.driverModel.vehicle_no;
        dict["address"] = this.driverModel.address;
        dict["address_details"] = this.driverModel.address_details;
        dict["image"] = this.driverModel.profilePic;
        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.addDriver(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    updateDriverAPI() {
        var dict = {};
        dict["firstname"] = this.driverModel.firstname;
        dict["lastname"] = this.driverModel.lastname;
        dict["email"] = this.driverModel.email;
        dict["phone"] = this.driverModel.phone;
        dict["extra_phone"] = this.driverModel.extra_phone;
        dict["licence_no"] = this.driverModel.licence_no;
        dict["vehicle_no"] = this.driverModel.vehicle_no;
        dict["address"] = this.driverModel.address;
        dict["address_details"] = this.driverModel.address_details;

        if (!this.validURL(this.driverModel.profilePic)) {
            dict["image"] = this.driverModel.profilePic;
        }
        dict["id"] = this.driverModel.driverID;
        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.updateDriver(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    cancelClick() {
        this.navctrl.pop();
    }


    validURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    }


}