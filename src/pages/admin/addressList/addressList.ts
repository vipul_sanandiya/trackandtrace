import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, App } from 'ionic-angular';
import { WebService } from '../../../services/webService';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from '../../../services/constant';
import { CallNumber } from '@ionic-native/call-number';
// import { TrackLoad } from '../trackLoad/trackLoad';
import { AddAddressPage } from '../AddAddress/AddAddress';


@Component({
    selector: 'page-addressList',
    templateUrl: 'addressList.html'
})
export class AddressList {
    
    addressListArry = [];
    currentPagination = 1;
    infiniteScrollEnable = true;
    indexExpandView = 10000;   

    searchModel: any;

    constructor(public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public callNumber: CallNumber, public app:App, public alertCtrl:AlertController) {
        

    }

    
    ionViewWillEnter() {
        this.currentPagination = 1;
        this.infiniteScrollEnable = true;
        this.indexExpandView = 1000;
        this.addressListArry = [];
        this.AddressListAPI();
    }

    AddressListAPI() {
        var dict = {};
        dict["page"] = this.currentPagination;

        this.constant.LoadingPresent();
        this.Service.getAddresslist(dict).subscribe((result) => {

            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.addressListArry.push(tempData[index]);
                }
                console.log('addressListArry=', this.addressListArry);
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    InfinitScrollingAPI(infiniteScroll: any) {

        console.log('InfinitScrollingAPI');
        var dic = {};
        this.currentPagination = this.currentPagination + 1
        dic['page'] = this.currentPagination;

        this.Service.getAddresslist(dic).subscribe((result) => {
            infiniteScroll.complete();
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.addressListArry.push(tempData[index]);
                }
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
            if (result.data.has_more_pages) {
                this.infiniteScrollEnable = true;
            } else {
                this.infiniteScrollEnable = false;
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    searchChangeEvent() {
        var timer: any;
        clearTimeout(timer);
        console.log(this.searchModel);
        if (this.searchModel && this.searchModel != '' && this.searchModel != null) {
            timer = setTimeout(x => {
                this.SearchAddressAPI();
            }, 1500);
        }
    }

    SearchAddressAPI() {
        var dict = {};
        dict["search_text"] = this.searchModel;
        this.Service.getAddresslist(dict).subscribe((result) => {
            this.addressListArry = [];
            console.log(result);
            if (result.status) {
                var tempData = result.data.records;
                for (let index = 0; index < tempData.length; index++) {
                    this.addressListArry.push(tempData[index]);
                }
                console.log('addressListArry=', this.addressListArry);
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    onCancelSearch() {
        console.log('cancel search');
        this.addressListArry = [];
        this.ionViewWillEnter();
    }

    expandViewClick(indexss) {
        if (this.indexExpandView == indexss) {
            this.indexExpandView = 10000;
        } else {
            this.indexExpandView = indexss;
        }
    }

    deleteAddressClick(items){
        let alert = this.alertCtrl.create({
            title: 'Action Alert',
            message: 'Are you sure you want to delete this address?',
            buttons: [
                {
                    text: 'Yes',
                    role: 'cancel',
                    handler: () => {
                        this.deleteAddress(items);
                    }
                },
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alert.present();
    }

    deleteAddress(items) {
        console.log(items);
        var dict = {};
        dict["id"] = items.id;
        this.constant.LoadingPresent();
        this.Service.deleteAddress(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.ionViewWillEnter();
            }
            this.constant.ToastCustom(result.message, 'bottom');
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }
    
    editAddressClick(address) {
        console.log(address);
        this.navctrl.push(AddAddressPage, { AddressID: address });
    }

    addAddressClick(){
        console.log('AddNewAddressClick');
        this.navctrl.push(AddAddressPage);
    }
}
 