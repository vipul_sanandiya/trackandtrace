import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, MenuController, App, Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { WebService } from '../../../services/webService';
import { Constant } from '../../../services/constant';
import { LoginPage } from '../../login/login/login';
import { EditProfile } from '../editProfile/editProfile';
import { ChangePassword } from '../changePassword/changePassword';
import { ANDROID_APP_VERSION, IOS_APP_VERSION } from '../../../services/config';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})
export class Settings {

    profileData: any;
    appVersion = '';

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public app: App, public platform:Platform) {

    }
    
    ionViewWillEnter(){
        
        this.profileData = this.constant.getUserData();
        console.log(this.profileData);
        this.detectChange.detectChanges();

        if (this.platform.is('android')) {
            this.appVersion = ANDROID_APP_VERSION;
        } else {
            this.appVersion = IOS_APP_VERSION;
        }

    }

    editProfileClick(){
        this.navctrl.push(EditProfile);
    }

    LogoutClick() {
        localStorage.clear();
        this.app.getRootNav().setRoot(LoginPage);
    }
    
    ChangePasswordClick(){
        this.navctrl.push(ChangePassword);
    }

}
