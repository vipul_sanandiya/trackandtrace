import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Constant } from '../../../services/constant';
import { WebService } from '../../../services/webService';

@Component({
  selector: 'page-changePassword',
  templateUrl: 'changePassword.html',
})

export class ChangePassword {
 
  

  nCPasswordModal: any;
  nPasswordModal: any;  
  OldPasswordModal:any;

  

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, public constant: Constant, public detectChange: ChangeDetectorRef, public Service: WebService) {



  }



  ionViewWillEnter() {
    
  }

 
  NewPasswordSubmitClick() {
    
    if (!this.OldPasswordModal) {
      this.constant.ToastCustom("Please enter old password", "top");
    } else if (!this.nPasswordModal) {
      this.constant.ToastCustom("Please enter new password", "top");
    } else if (this.nPasswordModal.length < 5) {
      this.constant.ToastCustom("Password length should be more than 6 characters.", "top");
    } else if (!this.nCPasswordModal) {
      this.constant.ToastCustom("Please enter new confirm password", "top");
    } else if (this.nPasswordModal != this.nCPasswordModal) {
      this.constant.ToastCustom("Password and confirm password does't match.", "top");
    } else {
      console.log('Done');
      this.passwordChangeAPI();
    }
  }

  passwordChangeAPI() {

    var dic = {};
    dic['old_password'] = this.OldPasswordModal;
    dic['new_password'] = this.nPasswordModal;
    dic['confirm_password'] = this.nCPasswordModal;

    this.constant.LoadingPresent();
    this.Service.changeAdminPassword(dic).subscribe((result) => {
      this.constant.LoadingHide();
      console.log(result);
      if (result.status) {
        this.navCtrl.pop();
      }
      this.constant.ToastCustom(result.message, 'top');
    }, (error) => {
      console.log(error.json());
      this.constant.Logout(error);
    });
  }

  

}
