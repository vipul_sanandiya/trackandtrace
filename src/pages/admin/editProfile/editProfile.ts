import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, LoadingController, AlertController, Events, ModalController, MenuController, App, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { WebService } from '../../../services/webService';
import { Constant } from '../../../services/constant';
import { LoginPage } from '../../login/login/login';
import { ImageUpload } from '../../../helper/imageUpload/imageUpload';

@Component({
    selector: 'page-editProfile',
    templateUrl: 'editProfile.html'
}) 
export class EditProfile {


    userData: any;
    profileData: any;
    profileDataModel = {
        firstName: '',
        lastName: '',
        mobileNumber: '',
        extraNumber: '',
        email: '',
        profilePic: '',
        address: '',
        address_details: ''
    }

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events,
        public app: App, public dataParam: NavParams) {


    }

    ionViewWillEnter() {
        this.userData = this.constant.getUserData();
        console.log(this.userData);
        this.getProfileDataAPI();
    }

    getProfileDataAPI() {
        var dict = {};
        dict["id"] = this.userData.id;
        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.getAdminProfileData(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.profileData = result.data;
                this.dataOperation();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    dataOperation() {
        this.profileDataModel.firstName = this.profileData.firstname;
        this.profileDataModel.lastName = this.profileData.lastname;
        this.profileDataModel.mobileNumber = this.profileData.phone;
        this.profileDataModel.extraNumber = this.profileData.extra_phone;
        this.profileDataModel.email = this.profileData.email;
        this.profileDataModel.profilePic = this.profileData.image;
        this.profileDataModel.address = this.profileData.address;
        this.profileDataModel.address_details = this.profileData.address_details;
    }

    uploadProfilePIC() {

        let imageUploadModel = this.modalCtrl.create(ImageUpload);
        imageUploadModel.present();
        imageUploadModel.onDidDismiss(data => {
            console.log(data);
            if (data) {
                this.profileDataModel.profilePic = data;
            }
        });

    }

    saveClick() {

        if (!this.profileDataModel.firstName) {
            this.constant.ToastCustom('Please enter first name.', 'bottom');
        } else if (!this.profileDataModel.lastName) {
            this.constant.ToastCustom('Please enter last name.', 'bottom');
        } else if (!this.profileDataModel.mobileNumber) {
            this.constant.ToastCustom('Please enter mobile number.', 'bottom');
        } else if (!this.profileDataModel.email) {
            this.constant.ToastCustom('Please enter email address.', 'bottom');
        } else if (!this.constant.isValidEmail(this.profileDataModel.email)) {
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        } else {
            this.updateProfileDataAPI();
            console.log('Done');
        }
    }

    updateProfileDataAPI() {
        var dict = {};
        dict["firstname"] = this.profileDataModel.firstName;
        dict["lastname"] = this.profileDataModel.lastName;
        dict["email"] = this.profileDataModel.email;
        dict["phone"] = this.profileDataModel.mobileNumber;
        dict["extra_phone"] = this.profileDataModel.extraNumber;
        dict["address"] = this.profileDataModel.address;
        dict["address_details"] = this.profileDataModel.address_details;

        if (!this.constant.validURL(this.profileDataModel.profilePic)) { 
            dict["image"] = this.profileDataModel.profilePic;
        }
        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.adminProfileUpdate(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                localStorage.setItem('userData', JSON.stringify(result.data));
                this.event.publish('sidemenuDataSet');
                this.navctrl.pop();
            }
            this.constant.ToastCustom(result.message, 'bottom');
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }



}
