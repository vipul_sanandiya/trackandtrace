import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, MenuController, ModalController, Events, Platform } from 'ionic-angular';
import { Constant } from "../../../services/constant";
import { WebService } from "../../../services/webService";
import { TranslateService } from '@ngx-translate/core';
import { CustomerTab } from '../../customer/tabs/tabs';
import { DriverTab } from '../../driver/tabs/tabs';
import { IOS_APP_VERSION, ANDROID_APP_VERSION } from '../../../services/config';

@Component({
    templateUrl: 'register.html',
    selector: 'page-register'
})
export class RegisterPage {

    // passwordModal:any = '123456';
    // emailModal : any = 'david.frank@gmail.com';
    // selectRoleModel = 2;

    passwordModal: any = '';
    emailModal: any = '';
    selectRoleModel = 2;

    loginForm = {};

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events, public platform:Platform) {
        this.menu.swipeEnable(false);

    }

    selectRoleClick() {
        if (this.selectRoleModel == 2) {
            this.selectRoleModel = 3;
        } else {
            this.selectRoleModel = 2;
        }
    }


    SignInClick() {


        if (!this.loginForm['firstName']) {
            this.constant.ToastCustom('Please enter first name.', 'bottom');
        } else if (!this.loginForm['lastName']) {
            this.constant.ToastCustom('Please enter last name.', 'bottom');
        } else if (!this.loginForm['email']) {
            this.constant.ToastCustom('Please enter email address.', 'bottom');
        } else if (!this.constant.isValidEmail(this.loginForm['email'])) {
            this.constant.ToastCustom('Please enter valid email address.', 'bottom');
        } else if (!this.loginForm['contactNo']) {
            this.constant.ToastCustom('Please enter contact number.', 'bottom');
        } else if (!this.loginForm['password']) {
            this.constant.ToastCustom('Please enter password.', 'bottom');
        } else if (this.loginForm['password'].length < 5) {
            this.constant.ToastCustom('Password length should be more than 5 characters.', 'bottom');
        } else if (!this.loginForm['cPassword']) {
            this.constant.ToastCustom('Please enter confirm password.', 'bottom');
        } else if (this.loginForm['cPassword'] != this.loginForm['password']) {
            this.constant.ToastCustom("Password and Confirm Password does't match.", 'bottom');
        } else {
            this.LoginAPI();
            console.log('Done');
        }

    }


    LoginAPI() {
        var dict = {};
        dict["user_type_id"] = this.selectRoleModel;
        dict["firstname"] = this.loginForm['firstName'];
        dict["lastname"] = this.loginForm['lastName'];
        dict["email"] = this.loginForm['email'];
        dict["phone"] = this.loginForm['contactNo'];
        dict["address"] = this.loginForm['address'];
        dict["address_details"] = this.loginForm['addressDetail'];
        dict["password"] = this.loginForm['password'];
        dict["confirm_password"] = this.loginForm['cPassword'];
        
        if (this.constant.pushToken) {
            dict["push_token"] = this.constant.pushToken;
        } else {
            dict["push_token"] = 'Coming from web';
        }

        if (this.platform.is('android')) {
            dict["device_type"] = 'android';
            dict["version"] = ANDROID_APP_VERSION;
        } else {
            dict["device_type"] = 'ios';
            dict["version"] = IOS_APP_VERSION;
        }


        console.log(dict);

        this.constant.LoadingPresent();
        this.Service.Register(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.navctrl.pop();
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    loginStatusMethod(result) {
        localStorage.setItem('Token', result.data.token);
        localStorage.setItem('userData', JSON.stringify(result.data));
        if (result.data.user_type_id == 2) {
            localStorage.setItem('role', '2');
            this.navctrl.setRoot(DriverTab);
        }
        if (result.data.user_type_id == 3) {
            localStorage.setItem('role', '3');
            this.navctrl.setRoot(CustomerTab);
        }

    }


    logDrag(eventss) {
        console.log(eventss);
    }

}