import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Constant } from '../../../services/constant';
import { WebService } from '../../../services/webService';

@Component({
  selector: 'page-resetPassword',
  templateUrl: 'resetPassword.html',
})

export class ResetPassword {

  OTPModelOne = '';
  OTPModelTwo = '';
  OTPModelThree = '';
  OTPModelFour = '';
  emailModel: any;

  nCPasswordModal: any;
  nPasswordModal: any;

  isemailContainerShow = true;
  isOTPContaienrShow = false;
  isResendOTPButtonShow = true;
  isNewPasswordContaienrShow = false;

  genratedOTPByAPI = '';


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, public constant: Constant, public detectChange: ChangeDetectorRef, public Service: WebService) {



  }



  ionViewWillEnter() {
    this.OTPModelOne = '';
    this.OTPModelTwo = '';
    this.OTPModelThree = '';
    this.OTPModelFour = '';
    this.emailModel= '';

    this.nCPasswordModal = '';
    this.nPasswordModal= '';

    this.isemailContainerShow = true;
    this.isOTPContaienrShow = false;
    this.isResendOTPButtonShow = true;
    this.isNewPasswordContaienrShow = false;

    this.genratedOTPByAPI = '';
  }

  GetOTPClick() {
    if (!this.emailModel) {
      this.constant.ToastCustom("Please enter your Email address.", "top");
    } else if (!this.constant.isValidEmail(this.emailModel)) {
      this.constant.ToastCustom("Please enter valid Email address.", "top");
    } else {
      console.log('Done');
      this.GetOTPAPI();
    }
  }

  ResendOTPClick() {
    this.isResendOTPButtonShow = true;
    setTimeout(x => {
      this.isResendOTPButtonShow = false;
    }, 30000);
    this.GetOTPAPI();
  }


  GetOTPAPI() {
    var dic = {};
    dic['email'] = this.emailModel;

    this.constant.LoadingPresent();
    this.Service.getEmailOTP(dic).subscribe((result) => {
      this.constant.LoadingHide();
      console.log(result);
      if (result.status) {
        this.isemailContainerShow = false;
        this.isOTPContaienrShow = true;
        this.genratedOTPByAPI = result.code;
        setTimeout(x => {
          this.isResendOTPButtonShow = false;
        }, 10000);
      }
      this.constant.ToastCustom(result.msg, 'top');
    }, (error) => {
      console.log(error.json());
      this.constant.Logout(error);
    });
  }

  enterCustomeKeyboard(otpValue) {
    console.log(otpValue);
    if (otpValue == 'clear') {
      this.OTPModelOne = '';
      this.OTPModelTwo = '';
      this.OTPModelThree = '';
      this.OTPModelFour = '';
    } else if (otpValue == 'back') {

      if (this.OTPModelFour != '') {
        this.OTPModelFour = '';
      } else if (this.OTPModelThree != '') {
        this.OTPModelThree = '';
      } else if (this.OTPModelTwo != '') {
        this.OTPModelTwo = '';
      } else if (this.OTPModelOne != '') {
        this.OTPModelOne = '';
      }

    } else {
      if (this.OTPModelOne == '') {
        this.OTPModelOne = otpValue;
      } else if (this.OTPModelTwo == '') {
        this.OTPModelTwo = otpValue;
      } else if (this.OTPModelThree == '') {
        this.OTPModelThree = otpValue;
      } else if (this.OTPModelFour == '') {
        this.OTPModelFour = otpValue;
      }
    }
  }


  VerifyOTPClick() {
    if (this.OTPModelOne && this.OTPModelTwo && this.OTPModelThree && this.OTPModelFour) {
      var enterOTP = this.OTPModelOne.toString() + this.OTPModelTwo.toString() + this.OTPModelThree.toString() + this.OTPModelFour.toString();
      console.log(enterOTP);
      if (this.genratedOTPByAPI == enterOTP) {
        this.isOTPContaienrShow = false;
        this.isNewPasswordContaienrShow = true;
      } else {
        this.constant.ToastCustom("Please enter valid OTP.", "top");
      }

    } else {
      this.constant.ToastCustom("Please enter OTP.", "top");
    }
  }

  NewPasswordSubmitClick() {
    if (!this.nPasswordModal) {
      this.constant.ToastCustom("Please enter new password", "top");
    } else if (this.nPasswordModal.length < 5) {
      this.constant.ToastCustom("Password length should be more than 6 characters.", "top");
    } else if (!this.nCPasswordModal) {
      this.constant.ToastCustom("Please enter new confirm password", "top");
    } else if (this.nPasswordModal != this.nCPasswordModal) {
      this.constant.ToastCustom("Password and confirm password does't match.", "top");
    } else {
      console.log('Done');
      this.passwordUpdateAPI();
    }
  }

  passwordUpdateAPI() {
    var enterOTP = this.OTPModelOne.toString() + this.OTPModelTwo.toString() + this.OTPModelThree.toString() + this.OTPModelFour.toString();
    var dic = {};
    dic['email'] = this.emailModel;
    dic['code'] = enterOTP;
    dic['password'] = this.nCPasswordModal;
    dic['confirm_password'] = this.nCPasswordModal;

    this.constant.LoadingPresent();
    this.Service.updatePassword(dic).subscribe((result) => {
      this.constant.LoadingHide();
      console.log(result);
      if (result.status) {
        this.navCtrl.pop();
      }
      this.constant.ToastCustom(result.msg, 'top');
    }, (error) => {
      console.log(error.json());
      this.constant.Logout(error);
    });
  }

}
