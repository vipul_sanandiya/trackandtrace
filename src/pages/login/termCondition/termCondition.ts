import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, MenuController, ModalController, Events } from 'ionic-angular';
import { Constant } from "../../../services/constant";
import { WebService } from "../../../services/webService";
import { TranslateService } from '@ngx-translate/core';
import { CustomerTab } from '../../customer/tabs/tabs';
import { DriverTab } from '../../driver/tabs/tabs';

@Component({
    templateUrl: 'termCondition.html',
    selector: 'page-termCondition'
})
export class TermCondition {

    messageModel = '';
    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public event: Events) {
        this.menu.swipeEnable(false);

    }

    ionViewWillEnter(){
        this.getTermAndCondition();
    }


    getTermAndCondition() {
        
        this.constant.LoadingPresent();
        this.Service.getTermCondition().subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.messageModel = result.data;
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

   

}