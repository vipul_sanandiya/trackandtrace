import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, MenuController, ModalController, Events, Platform, AlertController } from 'ionic-angular';
import { Constant } from "../../../services/constant";
import { WebService } from "../../../services/webService";
import { TranslateService } from '@ngx-translate/core';
import { CustomerTab } from '../../customer/tabs/tabs';
import { DriverTab } from '../../driver/tabs/tabs';
import { RegisterPage } from '../register/register';
import { LocationTracker } from '../../../services/locationTracker';
import { ResetPassword } from '../resetPassword/resetPassword';
import { TermCondition } from '../termCondition/termCondition';
import { LoadList } from '../../admin/loadList/loadList';
import { IOS_APP_VERSION, ANDROID_APP_VERSION } from '../../../services/config';

@Component({
    templateUrl: 'login.html',
    selector: 'page-login'
})
export class LoginPage {

    passwordModal: any = '';
    emailModal: any = '';

    // driver
    // emailModal: any = 'driver@faktrack.com';
    
    // customer
    // emailModal: any = 'customer@faktrack.com';

    // Admin
    // emailModal: any = 'admin@faklogistics.com';
    // emailModal: any = 'admin@faktrack.com';
    

    // Dispacher
    // emailModal: any = 'dispatcher@faktrack.com';

    // passwordModal: any = 'faktrack@2020';

    constructor(private menu: MenuController, public navctrl: NavController, public Service: WebService, public detectChange: ChangeDetectorRef,
        public constant: Constant, public modalCtrl: ModalController, public translate: TranslateService, public platform: Platform, public locationTracker: LocationTracker,
        public alertCtrl: AlertController, public event: Events) {
        this.menu.swipeEnable(false);

        // if (this.platform.is('ios')) {
        //     this.locationTracker.startTrackingWorkingIOS(8);
        // }else{
        //     this.locationTracker.startTrackingTwo();
        // }


    }



    SignInClick() {


        if (!this.emailModal) {
            this.constant.ToastCustom('Please enter your email address or phone number.', 'bottom');
        } else if (!this.passwordModal) {
            this.constant.ToastCustom('Please enter your password.', 'bottom');
        } else {
            this.LoginAPI();
        }

    }


    LoginAPI() {
        var dict = {};
        dict["username"] = this.emailModal;
        dict["password"] = this.passwordModal;
        if (this.constant.pushToken) {
            dict["push_token"] = this.constant.pushToken;
        } else {
            dict["push_token"] = 'Coming from web';
        }

        if (this.platform.is('android')) {
            dict["device_type"] = 'android';
            dict["version"] = ANDROID_APP_VERSION;
        } else {
            dict["device_type"] = 'ios';
            dict["version"] = IOS_APP_VERSION;
        }



        this.constant.LoadingPresent();
        this.Service.Login(dict).subscribe((result) => {
            this.constant.LoadingHide();
            console.log(result);
            if (result.status) {
                this.loginStatusMethod(result);
            } else {
                this.constant.ToastCustom(result.message, 'bottom');
            }
        }, (error) => {
            console.log(error.json());
            this.constant.Logout(error);
        });
    }

    loginStatusMethod(result) {

        if (result.data.user_type_id == 1) {
            localStorage.setItem('role', '1');
            localStorage.setItem('Token', result.data.token);
            localStorage.setItem('userData', JSON.stringify(result.data));
            this.event.publish('sidemenuDataSet');
            this.navctrl.setRoot(LoadList);
        }
        if (result.data.user_type_id == 2) {
            localStorage.setItem('role', '2');
            localStorage.setItem('Token', result.data.token);
            localStorage.setItem('userData', JSON.stringify(result.data));
            this.navctrl.setRoot(DriverTab);
        }
        if (result.data.user_type_id == 3) {
            localStorage.setItem('role', '3');
            localStorage.setItem('Token', result.data.token);
            localStorage.setItem('userData', JSON.stringify(result.data));
            this.navctrl.setRoot(CustomerTab);
        }

        // Dispacher
        if (result.data.user_type_id == 4) {
            localStorage.setItem('role', '4');
            localStorage.setItem('Token', result.data.token);
            localStorage.setItem('userData', JSON.stringify(result.data));
            this.event.publish('sidemenuDataSet');
            this.navctrl.setRoot(LoadList);
        }

    }


    RegisterClick() {
        this.navctrl.push(RegisterPage);
    }

    resetPassword() {
        this.navctrl.push(ResetPassword);
    }

    TermAndCondtionClick() {
        this.navctrl.push(TermCondition);
    }
}