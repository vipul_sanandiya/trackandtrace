import { Component, ChangeDetectorRef } from '@angular/core';
import { ViewController, ModalController, NavParams, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Constant } from "../../services/constant";
import { Network } from '@ionic-native/network';

import { Platform } from 'ionic-angular/platform/platform';
import { TranslateService } from '@ngx-translate/core';
import { WebService } from '../../services/webService';

@Component({
  templateUrl: 'ImageUpload.html',
})

export class ImageUpload {

  isPopupShown = true;
  userData: any = [];

  constructor(public ParamData: NavParams, public viewCtrl: ViewController, public modalCtrl: ModalController,
    public constant: Constant, public actionSheetCtrl: ActionSheetController, private camera: Camera,
    public platform: Platform, public translate: TranslateService,
    public cdRef: ChangeDetectorRef, public network: Network, public service:WebService) {

    this.InternetDiscconectMethod();

  }

  CameraClick() {
    this.isPopupShown = false;

    if (this.platform.is('cordova')) {
      this.OpenCameara();  
    }else{
      this.viewCtrl.dismiss();
    }
    
  }

  GalleryClick() {
    this.isPopupShown = false;
    if (this.platform.is('cordova')) {
      this.OpenGallery();
    }else{
      this.viewCtrl.dismiss();
    }
  }

  // From CAMERA
  OpenCameara() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      targetWidth: 750,
      targetHeight: 750
    }

    var self = this;
    this.camera.getPicture(options).then((imageData) => {
      console.log('ImageData=>', imageData);
      var baseImageData = "data:image/jpeg;base64," + imageData;
      this.viewCtrl.dismiss(baseImageData);
    }, (err) => {
      console.log('Camera_Cancel_err=>', err);
      this.viewCtrl.dismiss(false);
    });

  }

  // From GALLERY
  OpenGallery() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.ALLMEDIA,
      correctOrientation: true,
      targetWidth: 750,
      targetHeight: 750,
      saveToPhotoAlbum: true
    }

    var self = this;
    this.camera.getPicture(options).then((imageData) => {
      console.log('ImageData=>', imageData);
      var baseImageData = "data:image/jpeg;base64," + imageData;
      this.viewCtrl.dismiss(baseImageData);
    }, (err) => {
      console.log('Gallery_Cancel_err=>', err);
      this.viewCtrl.dismiss(false);
    });
  }

  
  CloseClick() {
    this.viewCtrl.dismiss(false);
  }

  InternetDiscconectMethod() {
    this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.viewCtrl.dismiss(false);
    });
  }

}
