import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { Constant } from "../../services/constant";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-no-internet-connection',
  templateUrl: 'no-internet-connection.html',
})
export class NoInternetConnectionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public network:Network,
  public viewCtrl:ViewController, public constant:Constant, public translate:TranslateService) {
    
  }

  ionViewDidLoad() {
    var self = this;
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      self.constant.isNetworkAvailablePage = false;
      self.viewCtrl.dismiss();
    });
    console.log(connectSubscription);
  }

  TryAgainClick(){
  this.translate.get("NO_INTERNET.NO_INTERNET_ERROR").subscribe((res: string) => {
      this.constant.ToastCustom(res['NO_INTERNET.NO_INTERNET_ERROR'], 'top');    
    });
  }

}
