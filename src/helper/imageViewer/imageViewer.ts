import { Component } from '@angular/core';
import { ViewController, ModalController, NavParams, LoadingController } from 'ionic-angular';

@Component({
  templateUrl: 'imageViewer.html'
})
export class ImageViewer {
  
  imageURL:any = [];
  constructor(public ParamData: NavParams, public viewCtrl: ViewController, public modalCtrl: ModalController,
  public Loading:LoadingController) {

      this.imageURL = this.ParamData.get('url');
      console.log(this.imageURL);

  }

  CloseModelAction() {
    this.viewCtrl.dismiss();
  }


 
  
}


