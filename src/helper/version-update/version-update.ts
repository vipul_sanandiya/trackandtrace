import { Component } from '@angular/core';
import { Platform, NavController, NavParams } from 'ionic-angular';
import { ANDROID_APP_VERSION, IOS_APP_VERSION } from '../../services/config';
import { TranslateService } from '@ngx-translate/core';
import { Market } from '@ionic-native/market';

@Component({
  selector: 'page-version-update',
  templateUrl: 'version-update.html',
})
export class VersionUpdatePage {

  paramData: any;
  featureListArry = [];
  updatedVersion = '';
  currentVersion = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public translate: TranslateService, public market:Market) {

  }

  ionViewDidLoad() {
    this.paramData = this.navParams.get('data');
    console.log(this.paramData);

    console.log('ionViewDidLoad VersionUpdatePage');
    if (this.platform.is('ios')) {
      this.currentVersion = IOS_APP_VERSION;
    } else {
      this.currentVersion = ANDROID_APP_VERSION;
    }

    console.log(this.currentVersion);
    this.featureListArry = this.paramData.features;
    this.updatedVersion = this.paramData.version;

  }

  updateAppClick() {

    if (this.platform.is('ios')) {
      this.market.open('1489446582');
    }else{
      this.market.open('com.fak.tracktrace');
    }
    
  }

}
