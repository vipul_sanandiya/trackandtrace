
import { Injectable, NgZone } from '@angular/core';
import 'rxjs/add/operator/filter';
import { Constant } from './constant';
import { Network } from '@ionic-native/network';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class SqliteDatabase {


    db: any;
    internetDB:any;

    savedLocationArry = [];
    savedInternetArry = [];

    constructor(public zone: NgZone, public constant: Constant, public network: Network, private sqlite: SQLite) {


    }

    //----------------------------------------Location Database---------------------------------------------//
    //----------------------------------------Location Database---------------------------------------------//

    createTable() {
        console.log('------------------createTable SQlite Table Create call-----------------');
        this.sqlite.create({ name: 'location.db', location: 'default' }).then((db: SQLiteObject) => {
            console.log(db);
            this.db = db;
            var sql = 'create table IF NOT EXISTS `location` (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, order_id INT, comment VARCHAR(255), latitude FLOAT, longitude FLOAT)';
            this.db.executeSql(sql, {}).then(() => {
                console.log('Execute Query', sql);
            }).catch(e => {
                console.log('Execute Query Error', JSON.stringify(e));
            });
        }).catch(e => {
            console.log(e)
        });
    }

    saveLocation(data) {
        console.log('------------------saveLocation SQlite Table -----------------');
        console.log('saveLocation call', data);

        var stringConvert = "'" + data['comment'] + "'"
        this.sqlite.create({ name: 'location.db', location: 'default' }).then((db: SQLiteObject) => {
            var sql = 'INSERT INTO location (order_id,comment,latitude,longitude) VALUES(' + data['order_id'] + ',' + stringConvert + ',' + data['latitude'] + ',' + data['longitude'] + ')';
            console.log(sql);
            db.executeSql(sql).then(res => {
                console.log(res);
            }).catch(e => {
                console.log(e);
            });
        }).catch(e => {
            console.log(e);
        });

    }

    getCurrentLocation() {
        console.log('------------------getCurrentLocation SQlite Table -----------------');
        this.sqlite.create({ name: 'location.db', location: 'default' }).then((db: SQLiteObject) => {
            db.executeSql('SELECT * FROM location').then(res => {
                console.log(res);
                for (var i = 0; i < res.rows.length; i++) {
                    this.savedLocationArry.push(res.rows.item(i));
                }
                console.log(this.savedLocationArry);
            }).catch(e => {
                console.log('Error on getCurrentLocation', e);
            });
        }).catch(e => {
            console.log('Error on getCurrentLocation 2', e);
        });
    }

    getData() {

        return new Promise((resolve, reject) => {
            this.db.executeSql('SELECT * from location', []).then((data) => {
                this.savedLocationArry = [];
                if (data.rows.length > 0) {
                    for (let i = 0; i < data.rows.length; i++) {
                        this.savedLocationArry.push(data.rows.item(i));
                    }
                }
                resolve(this.savedLocationArry);
            }, (error) => {
                reject(error);
            })
        });
    }

    deleteRecord() {
        this.sqlite.create({ name: 'location.db', location: 'default' }).then((db: SQLiteObject) => {
            db.executeSql('DELETE FROM location').then(res => {
                console.log(res);
            }).catch(e => {
                console.log(e);
            });
        }).catch(e => {
            console.log(e);
        });
    }


    //----------------------------------------Internet Database---------------------------------------------//
    //----------------------------------------Internet Database---------------------------------------------//

    
    createTableInternet() {
        this.sqlite.create({ name: 'internet.db', location: 'default' }).then((db: SQLiteObject) => {
            console.log(db);
            this.internetDB = db;
            var sql = 'create table IF NOT EXISTS `internet` (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, label VARCHAR(255), version VARCHAR(255))';
            this.internetDB.executeSql(sql, {}).then(() => {
                console.log('Execute Query', sql);
            }).catch(e => {
                console.log('Execute Query Error', JSON.stringify(e));
            });
        }).catch(e => {
            console.log(e)
        });
    }

    saveInternetData(data) {

        var stringConvert = "'" + data['label'] + "'";
        var stringConvertTwo = "'" + data['version'] + "'";

        this.sqlite.create({ name: 'internet.db', location: 'default' }).then((db: SQLiteObject) => {
            var sql = 'INSERT INTO internet (label,version) VALUES(' + stringConvert + ',' + stringConvertTwo + ')';
            console.log(sql);
            db.executeSql(sql).then(res => {
                console.log(res);
            }).catch(e => {
                console.log(e);
            });
        }).catch(e => {
            console.log(e);
        });
    }

    getDataInternet() {

        return new Promise((resolve, reject) => {
            this.internetDB.executeSql('SELECT * from internet', []).then((data) => {
                this.savedInternetArry = [];
                if (data.rows.length > 0) {
                    for (let i = 0; i < data.rows.length; i++) {
                        this.savedInternetArry.push(data.rows.item(i));
                    }
                }
                resolve(this.savedInternetArry);
            }, (error) => {
                reject(error);
            })
        });
    }

    deleteRecordInternet() {
        this.sqlite.create({ name: 'internet.db', location: 'default' }).then((db: SQLiteObject) => {
            db.executeSql('DELETE FROM internet').then(res => {
                console.log(res);
            }).catch(e => {
                console.log(e);
            });
        }).catch(e => {
            console.log(e);
        });
    }

}



