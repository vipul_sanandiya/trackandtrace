import { Injectable } from '@angular/core';
import { LoadingController, ToastController, AlertController, Events, Platform, ModalController } from 'ionic-angular';
import { Http, RequestOptions, Headers } from '@angular/http';
import { LoadingCustom } from './lodingCustom';
import { BASE_URL } from './config';
import { DatePipe } from '@angular/common';

@Injectable()

export class Constant {

    isNetworkAvailablePage = false;
    isSelectedTabIndex = '0';
    pushToken = ''; 
    IMAGE_EXTENTION_ARRY = ['png', 'jpg', 'jpeg'];
    DOCUMENT_UPLOAD_SIZE_LIMIT = 10;
    UPLOAD_IMAGE_PARAM = {};
    UPLOAD_IMAGE_URL = "";
    BASE_URL = BASE_URL;
    TimeInterval_Android = '';
    TimeInterval_IOS = '';

    timeArry = [{
        time:'00:00 AM',
        value : '00:00 AM'
    },{
        time:'1:00 AM',
        value : '1:00 AM'
    },{
        time:'2:00 AM',
        value : '2:00 AM'
    },{
        time:'3:00 AM',
        value : '3:00 AM'
    },{
        time:'4:00 AM',
        value : '4:00 AM'
    },{
        time:'5:00 AM',
        value : '5:00 AM'
    },{
        time:'6:00 AM',
        value : '6:00 AM'
    },{
        time:'7:00 AM',
        value : '7:00 AM'
    },{
        time:'8:00 AM',
        value : '8:00 AM'
    },{
        time:'9:00 AM',
        value : '9:00 AM'
    },{
        time:'10:00 AM',
        value : '10:00 AM'
    },{
        time:'11:00 AM',
        value : '11:00 AM'
    },{
        time:'12:00 PM',
        value : '12:00 PM'
    },{
        time:'1:00 PM',
        value : '1:00 PM'
    },{
        time:'2:00 PM',
        value : '2:00 PM'
    },{
        time:'3:00 PM',
        value : '3:00 PM'
    },{
        time:'4:00 PM',
        value : '4:00 PM'
    },{
        time:'5:00 PM',
        value : '5:00 PM'
    },{
        time:'6:00 PM',
        value : '6:00 PM'
    },{
        time:'7:00 PM',
        value : '7:00 PM'
    },{
        time:'8:00 PM',
        value : '8:00 PM'
    },{
        time:'9:00 PM',
        value : '9:00 PM'
    },{
        time:'10:00 PM',
        value : '10:00 PM'
    },{
        time:'11:00 PM',
        value : '11:00 PM'
    }];


    constructor(public Loading: LoadingController, public toastCtrl: ToastController, public datePipe: DatePipe,
        public alertCtrl: AlertController, public loadingCustom: LoadingCustom, public Event: Events, public Platform: Platform, public http: Http,
        public modalCtrl: ModalController) {
    }


    //Number,alphabet allowed 
    AlphabetNumberPettern(pettern) {
        var regExp = /^[0-9a-zA-Z]+$/;
        if (!regExp.test(pettern)) {
            return false;
        }
        return true;
    }

    //character validation Number [0-9],alphabet
    CharacterValidation(pettern) {
        var regExp = /^[0-9a-zA-Z ]+$/;
        if (!regExp.test(pettern)) {
            return false;
        }
        return true;
    }

    //Number,alphabet,space allowed
    BusinessNamePettern(pettern) {
        var regExp = /^[0-9a-zA-Z ]+$/;
        if (!regExp.test(pettern)) {
            return false;
        }
        return true;
    }

    BlackSpaceCheck(pettern) {
        var regExp = /.*[^ ].*/;
        if (!regExp.test(pettern)) {
            return false;
        }
        return true;
    }


    isValidPassword(pattern) {
        var regExp = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)\S*$/;
        if (!regExp.test(pattern)) {
            return false;
        }
        return true;
    }

    //Firest Time space alloewd
    firstTimeSpaceNoteallowed(pattern) {
        var regExp = /\S/;
        if (!regExp.test(pattern)) {
            return false;
        }
        return true;
    }




    // Mobile Validation
    isValidMobile(mobile) {
        var regExp = /^[0-9]{10}$/;
        if (!regExp.test(mobile)) {
            return false;
        }
        return true;
    }

    isValidAddUpedateCon(mobile) {
        var regExp = /^[0-9]/;
        if (!regExp.test(mobile)) {
            return false;
        }
        return true;
    }

    isValidEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
            return false;
        } else {
            return true;
        }
    }

    isValidCity(city) {
        var re = /[a-zA-Z._^%$#!~@,-]+./;
        if (!re.test(city)) {
            return false;
        } else {
            return true;
        }
    }

    // Get current User data
    getUserData() {
        var data = JSON.parse(localStorage.getItem('userData'));
        if (data) {
            return data;
        } else {
            return null;
        }
    }


    // Alert
    AlertCustom(Titles, Message, BtnTitle) {
        var Title = Titles;
        let alert = this.alertCtrl.create({
            title: Title,
            message: Message,
            buttons: [
                {
                    text: BtnTitle,
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },]
        });
        alert.present();
    }
    
    validURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    }

    // Toast Message
    ToastCustom(message, positions) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 2500,
            position: positions
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }


    // Loading
    LoadingPresent() {
        this.loadingCustom.LoadingPresent(+1);
    }

    LoadingHide() {
        this.loadingCustom.LoadingHide();
    }

    // Logout
    Logout(error) {
        this.LoadingHide();
        console.log(error);
        console.log(error.status);

        // if (error.status == 400) {
        //     this.LogoutAlert();
        // }

        if (error.status == 400) {
            localStorage.removeItem('Token');
            localStorage.removeItem('UserInfo');
            this.Event.publish('LogoutEvent');
        }
    }

    LogoutAlert() {
        let alert = this.alertCtrl.create({
            title: 'Server Error',
            message: 'Please try again after sometimes.',
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alert.present();
    }


    dateConvertFormate(date, formate) {
        return this.datePipe.transform(date, formate);
    }

    GetTimespToDate(dateObj) {
        var ts = new Date(dateObj);
        return ts.toLocaleDateString('en-GB');
    }

    GetTimeStampFromDate(dateObj) {
        return new Date(dateObj).getTime();
    }


    CheckValueInArry(tempArry, TextMsg) {
        return tempArry.indexOf("TextMsg");
    }

    doubleToFloat(value) {
        var valueToFixed = value.toFixed(2);
        return parseFloat(valueToFixed);
    }

    replceString(value) {
        return value.replace(",", " | ");
    }


    GetTimeDiffrence(NotiTime) {
        var currentDateinMili = new Date().valueOf();
        var TotalMili = currentDateinMili - NotiTime;
        var TotalSec = TotalMili / 1000;
        var TotalMin = TotalSec / 60;
        var TotalHour = TotalMin / 60;
        var TotalDays = TotalHour / 24
        var TotalMonth = TotalDays / 30
        //console.log(TotalSec);
        //console.log(TotalMin);
        //console.log(TotalHour);  

        var date = new Date(NotiTime);
        var dateToStr = date.toUTCString().split(' ');
        var cleanDate = dateToStr[1] + ' ' + dateToStr[2];

        //console.log(cleanDate);

        if (TotalSec < 60) {
            var SecondString = 'Just Now';
            return SecondString;
        } else if (TotalMin < 60) {
            var intvalueMin = Math.floor(TotalMin);
            var MinuteinString = intvalueMin + ' mins ago'
            return MinuteinString;
        } else if (TotalHour < 24) {
            var intvalueHour = Math.floor(TotalHour);
            var HourString = intvalueHour + ' hour ago'
            return HourString;
        } else if (TotalDays < 30) {
            var intvalueDay = Math.floor(TotalDays);
            var DayString = intvalueDay + ' days ago'
            return DayString;
        } else if (TotalMonth < 12) {
            var intvalueMonth = Math.floor(TotalMonth);
            var MonthString = intvalueMonth + ' month ago'
            return MonthString;
        } else {
            return cleanDate;
        }

    }




    DynamicInputFields: any;
    checkMaxLength(value) {
        // this.changeDetect.detectChanges();
        this.DynamicInputFields = value.length > 4 ? value.substring(0, 4) : value;
    }

    // FileExtesion Check
    FileExtesionCheck(filename) {
        return filename.substring(filename.lastIndexOf('.') + 1, filename.length) || filename;
    }


    checkDateValidation(fromDateModal, toDateModal) {
        if (fromDateModal && toDateModal) {
            var fromdateTime = this.GetTimeStampFromDate(fromDateModal);
            var todateTime = this.GetTimeStampFromDate(toDateModal);
            console.log(fromdateTime);
            console.log(todateTime);
            if (todateTime > fromdateTime || todateTime == fromdateTime) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }



    CovertFirstChar(valueOfString) {
        valueOfString = valueOfString.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        return valueOfString;
    }

    lowercaseConvertString(stringRendom) {
        return stringRendom.toLowerCase();
    }

    IsImage(format) {
        console.log(format);
        var isSupported = false;
        for (let index = 0; index < this.IMAGE_EXTENTION_ARRY.length; index++) {
            if (this.IMAGE_EXTENTION_ARRY[index].toLowerCase() == format.toLowerCase()) {
                isSupported = true;
            }
        }

        if (isSupported) {
            return true;
        } else {
            return false;
        }
    }

    loadStatus(StatusID) {
        
        // 1 Pending
        // 2 Accept
        // 3 Reject
        // 4 Going to pick - up
        // 5 Loading
        // 6 On the way
        // 7 Reach at drop point
        // 8 Unloading
        // 9 Delivered

        if (StatusID == 1) {
            return "pending";
        } else if (StatusID == 2) {
            return "accept";
        } else if (StatusID == 3) {
            return "reject";
        } else if (StatusID == 4) {
            return "goingToPickup";
        } else if (StatusID == 5) {
            return "loading";
        } else if (StatusID == 6) {
            return "onTheWay";
        } else if (StatusID == 7) {
            return "reachAtDropPoint";
        } else if (StatusID == 8) {
            return "unloading";
        } else {
            return "delivered";
        }
    }

    nextStatus(StatusID){

         // 1 Pending
        // 2 Accept
        // 3 Reject
        // 4 Going to pick - up
        // 5 Loading
        // 6 On the way
        // 7 Reach at drop point
        // 8 Unloading
        // 9 Delivered

        if (StatusID == 2) {
            return "Going to pick - up";
        } else if (StatusID == 4) {
            return "Loading";
        } else if (StatusID == 5) {
            return "On the way";
        } else if (StatusID == 6) {
            return "Reached Destination";
        } else if (StatusID == 7) {
            return "Unloading";
        } else if (StatusID == 8) {
            return "Delivered";
        } else {
            return "delivered";
        }
    }



}
