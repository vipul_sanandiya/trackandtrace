import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, RequestOptions, Headers } from '@angular/http';
import { WebServiceHandler } from './webServiceHandler';
import { Constant } from './constant';
import { BASE_URL } from './config';

@Injectable()

export class WebService {


    constructor(private constant: Constant, public WebserviceHandler: WebServiceHandler, private http: Http) {

    }


    getAppVersion() {
        var URL = BASE_URL + 'api/version';
        return this.WebserviceHandler.Get(URL).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Login
    Login(Parameter) {
        var URL = BASE_URL + 'api/check-login';
        return this.WebserviceHandler.Post(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Store app log
    StoreAppLog(Parameter) {
        var URL = BASE_URL + 'api/store-app-log';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Register
    Register(Parameter) {
        var URL = BASE_URL + 'api/create-profile';
        return this.WebserviceHandler.Post(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Update Profile
    UpdateProfile(Parameter) {
        var URL = BASE_URL + 'api/update-profile';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Order List
    OrderList(Parameter) {
        var URL = BASE_URL + 'api/order-list';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Notification List
    NotificationList(Parameter) {
        var URL = BASE_URL + 'api/notification-list';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Order Details
    OrderDetails(Parameter) {
        var URL = BASE_URL + 'api/order-detail';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Order Details
    UpdateOrder(Parameter) {
        var URL = BASE_URL + 'api/update-order';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //Order Details
    locationUpdate(Parameter) {
        var URL = BASE_URL + 'api/store-order-location';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    getTruckLocation(Parameter) {
        var URL = BASE_URL + 'api/order-locations';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    getEmailOTP(Parameter) {
        var URL = BASE_URL + '/api/send-forgot-password-link';
        return this.WebserviceHandler.Post(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    updatePassword(Parameter){
        var URL = BASE_URL + '/api/change-password';
        return this.WebserviceHandler.Post(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }


    getTermCondition() {
        var URL = BASE_URL + 'api/terms-conditions';
        return this.WebserviceHandler.Get(URL).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    changeLoadStatus(Parameter) {
        var URL = BASE_URL + 'api/update-load-status';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }
    


// ------------------------------Admin API---------------------------------------//
// ------------------------------Admin API---------------------------------------//


    readNotification(Parameter){
        var URL = BASE_URL + 'api/read-notification';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //------------------------------Load APIS---------------------------------------//

    searchCustomerList(Parameter){
        var URL = BASE_URL + 'api/search-customers';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    addLoad(Parameter){
        var URL = BASE_URL + 'api/store-load';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    searchLocationList(Parameter){
        var URL = BASE_URL + 'api/search-locations';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    searchDriverList(Parameter){
        var URL = BASE_URL + 'api/search-drivers';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    deleteLoad(Parameter){
        var URL = BASE_URL + 'api/delete-load';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    editLoad(Parameter){
        var URL = BASE_URL + 'api/view-edit-load-data';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //------------------------------Driver APIS---------------------------------------//

    addDriver(Parameter) {
        var URL = BASE_URL + 'api/driver-add';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    getLoadsMapPointList(Parameter) {
        var URL = BASE_URL + 'api/order-tracking-points';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }


    DriverList(Parameter){
        var URL = BASE_URL + 'api/driver-list'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }
    
    searchDriver(Parameter) {
        var URL = BASE_URL + 'api/search-drivers';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }
    
    editDriver(DriverID) {
        var URL = BASE_URL + 'api/driver-edit';
        console.log()
        return this.WebserviceHandler.PostWithHeader(URL, DriverID).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }
    
    updateDriver(Parameter) {
        var URL = BASE_URL + 'api/driver-update';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    deleteDriver(Parameter){
        var URL = BASE_URL + 'api/driver-delete';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    //------------------------------Shipper APIS---------------------------------------//

    ShipperList(Parameter){
        var URL = BASE_URL + 'api/customer-list'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    addShipper(Parameter) {
        var URL = BASE_URL + 'api/customer-add';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }
    
    editShipper(ShipperID) {
        var URL = BASE_URL + 'api/customer-edit';
        return this.WebserviceHandler.PostWithHeader(URL, ShipperID).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    deleteShipper(ShipperID){
        var URL = BASE_URL + 'api/customer-delete';  
        return this.WebserviceHandler.PostWithHeader(URL, ShipperID).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    updateShipper(parameter){
        var URL = BASE_URL + 'api/customer-update';  
        return this.WebserviceHandler.PostWithHeader(URL, parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }


    searchShipper(search_text) {
        var URL = BASE_URL + 'api/customer-list';
        return this.WebserviceHandler.PostWithHeader(URL, search_text).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    //------------------------------Address Menager APIS---------------------------------------//

    getAddresslist(Parameter){
        var URL = BASE_URL + 'api/address-manager-list';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    AddAddressManager(Parameter){
        var URL = BASE_URL + 'api/address-manager-add';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    deleteAddress(Parameter){
        var URL = BASE_URL + 'api/address-manager-delete';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    viewAddressDetails(Parameter){
        var URL = BASE_URL + 'api/address-manager-edit';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    updateAddressDetails(Parameter){
        var URL = BASE_URL + 'api/address-manager-update';
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        });
    }

    
    //------------------------------Admin User APIS---------------------------------------//

    UserList(Parameter){
        var URL = BASE_URL + 'api/admin-user-list'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    AddAdminuser(Parameter){
        var URL = BASE_URL + 'api/admin-user-add'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    EditAdminuser(Parameter){
        var URL = BASE_URL + 'api/admin-user-edit'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    DeleteAdminUser(Parameter){
        var URL = BASE_URL + 'api/admin-user-delete'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    UpdateAdminuser(Parameter){
        var URL = BASE_URL + 'api/admin-user-update'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    adminUserTypeList(Parameter){
        var URL = BASE_URL + 'api/admin-user-types-list'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    
    //------------------------------Admin Profile---------------------------------------//

    getAdminProfileData(Parameter){
        var URL = BASE_URL + 'api/account-setting'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    adminProfileUpdate(Parameter){
        var URL = BASE_URL + 'api/account-setting/update-profile'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    changeAdminPassword(Parameter){
        var URL = BASE_URL + 'api/account-setting/change-password'; 
        return this.WebserviceHandler.PostWithHeader(URL, Parameter).map(data => {
            return data.json();
        }, error => {
            return error;
        }); 
    }

    

}


