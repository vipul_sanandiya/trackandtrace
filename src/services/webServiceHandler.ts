import { Injectable } from '@angular/core';
import { Http ,RequestOptions, Headers} from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()

export class WebServiceHandler {

 
    constructor(private http: Http) {
    
    }

    

    Post(URL, Parameter) {
        
        let headers = new Headers({'Content-Type': 'application/json','Accept' : 'application/json'});
        var Option = new RequestOptions({headers:headers});

        return this.http.post(URL, Parameter, Option).map(data => {
            console.log('----------------',URL,'---------------------------');
            return data;
        }, error => {
            return error;
        });
    }
  

    PostWithHeader(URL, Parameter) {
        var currentLanguage =  localStorage.getItem('language');
        var accessToken =  localStorage.getItem('Token');
        let headers = new Headers({'Content-Type': 'application/json', 'token' : accessToken, 'language':currentLanguage});
        var Option = new RequestOptions({headers:headers});

        return this.http.post(URL, Parameter, Option).map(data => {
            console.log('----------------',URL,'---------------------------');
            return data;
        }, error => {
            return error;
        });
    }

    GetWithHeader(URL){
        var accessToken =  localStorage.getItem('Token');
        let headers = new Headers({'Content-Type': 'application/json', 'token' : accessToken});
        var Option = new RequestOptions({headers:headers});

        return this.http.get(URL, Option).map(data => {
            console.log('----------------',URL,'---------------------------');
            return data;
        }, error => {
            return error;
        }); 
    }
    

   
    Get(URL){
        let headers = new Headers({'Content-Type': 'application/json'});
        var Option = new RequestOptions({headers:headers});

        return this.http.get(URL, Option).map(data => {  
            console.log('-------------------------',URL,'---------------------------');
            return data;
        }, error => {
            console.log('WebserviceHandler=>'+error);
            return error;
        });

    }


  
}
