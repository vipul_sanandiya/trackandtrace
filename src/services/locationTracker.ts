
import { Injectable, NgZone } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse, BackgroundGeolocationEvents } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import 'rxjs/add/operator/filter';
import { WebService } from './webService';
import { Constant } from './constant';
import { BASE_URL, ANDROID_APP_VERSION, IOS_APP_VERSION } from './config';
import { Insomnia } from '@ionic-native/insomnia';
import { Network } from '@ionic-native/network';
import { SqliteDatabase } from './sqliteDatabase';
import { Platform } from 'ionic-angular';

@Injectable()
export class LocationTracker {

    public watch: any;
    public lat: number = 0;
    public lng: number = 0;

    updateLocation = true;
    intervalTemp: any;
    userData: any;
    isInternetConnected = true;

    constructor(public zone: NgZone, private backgroundGeolocation: BackgroundGeolocation, private geolocaction: Geolocation, private service: WebService,
        public constant: Constant, private insomnia: Insomnia, public network: Network, public sqliteDatabase: SqliteDatabase, public platform:Platform) {

        this.watch = '';
        this.userData = this.constant.getUserData();
        console.log(this.userData);
    }


    stopTracking() {
        clearInterval(this.intervalTemp);
        console.log('stopTracking', this.watch);
        try {
            if (this.watch !== '') {
                this.watch.unsubscribe();
                this.backgroundGeolocation.stop();
            } else {
                console.log('clear')
            }
        }
        catch (err) {
            console.log(err)
        }
    }

    startTrackingWorkingIOS(itesm) {

        console.log(parseInt(this.constant.TimeInterval_IOS));

        var accessToken = localStorage.getItem('Token');
        const config: BackgroundGeolocationConfig = {
            desiredAccuracy: 0,
            stationaryRadius: 20,
            distanceFilter: 1000,
            notificationTitle: 'Background tracking',
            notificationText: 'enabled',
            debug: false,
            interval: parseInt(this.constant.TimeInterval_IOS),
            fastestInterval: parseInt(this.constant.TimeInterval_IOS),
            activitiesInterval: parseInt(this.constant.TimeInterval_IOS),
            notificationsEnabled: false,
            syncUrl: 'https://www.faktrack.com/api/store-order-location',
            url: 'https://www.faktrack.com/api/store-order-location',
            httpHeaders: {
                'token': accessToken
            },
            // customize post properties
            postTemplate: {
                latitude: '@latitude',
                longitude: '@longitude',
                comment: 'IOS Plugin BG',
                order_id: itesm // you can also add your own properties
            }
        };

        this.backgroundGeolocation.checkStatus().then((status) => {
            console.log(status);
            console.log('status=' + status);
            this.backgroundGeolocation.start();

        });

        this.backgroundGeolocation.configure(config).then(() => {


            this.backgroundGeolocation.startTask().then((taskKey) => {
                console.log(taskKey);
                console.log('taskKey');

                this.backgroundGeolocation.headlessTask(async (event) => {
                    if (event.name === 'location' ||
                        event.name === 'stationary') {
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', 'https://www.faktrack.com/api/store-order-location');
                        xhr.setRequestHeader('token', accessToken);
                        xhr.send(JSON.stringify(event.params));
                    }
                });

                this.backgroundGeolocation.start();

            });
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {

            console.log(location);
            console.log('location=' + location);
        });


        this.backgroundGeolocation.on(BackgroundGeolocationEvents.error).subscribe((status: BackgroundGeolocationResponse) => {
            console.log('error=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.authorization).subscribe((status) => {
            console.log('authorization=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.abort_requested).subscribe((status) => {
            console.log('abort_requested=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.http_authorization).subscribe((status) => {
            console.log('http_authorization=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.background).subscribe((status: BackgroundGeolocationResponse) => {
            console.log('background=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.foreground).subscribe((status: BackgroundGeolocationResponse) => {
            console.log('background=' + status);

            this.backgroundGeolocation.getLocations().then((locations) => {
                console.log(locations);
                console.log('getLocations=' + locations);
            });
        });


    }

    startAndroidTracking(itesm) {

        this.insomnia.keepAwake().then(() => {
            console.log('success insomnia.keepAwake()')
        }, () => {
            console.log('error insomnia.keepAwake()')
        });


        var accessToken = localStorage.getItem('Token');
        const config: BackgroundGeolocationConfig = {
            desiredAccuracy: 0,
            stationaryRadius: 20,
            distanceFilter: 30,
            notificationTitle: 'Background tracking',
            notificationText: 'enabled',
            debug: false,
            interval: parseInt(this.constant.TimeInterval_Android),
            fastestInterval: parseInt(this.constant.TimeInterval_Android),
            activitiesInterval: parseInt(this.constant.TimeInterval_Android),
            notificationsEnabled: true,
            startForeground: true,
            syncUrl: 'https://www.faktrack.com/api/store-order-location',
            url: 'https://www.faktrack.com/api/store-order-location',
            httpHeaders: {
                'token': accessToken
            },
            // customize post properties
            postTemplate: {
                latitude: '@latitude',
                longitude: '@longitude',
                comment: 'Android Plugin BG',
                order_id: itesm // you can also add your own properties
            }
        };

        this.backgroundGeolocation.checkStatus().then((status) => {
            console.log(status);
            console.log('status=' + status);
            this.backgroundGeolocation.start();

        });

        this.backgroundGeolocation.configure(config).then(() => {


            this.backgroundGeolocation.startTask().then((taskKey) => {
                console.log(taskKey);
                console.log('taskKey');

                this.backgroundGeolocation.headlessTask(async (event) => {
                    if (event.name === 'location' ||
                        event.name === 'stationary') {
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', 'https://www.faktrack.com/api/store-order-location');
                        xhr.setRequestHeader('token', accessToken);
                        xhr.send(JSON.stringify(event.params));
                    }
                });

                this.backgroundGeolocation.start();

            });
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {

            console.log(location);
            console.log('location=' + location);
        });


        this.backgroundGeolocation.on(BackgroundGeolocationEvents.error).subscribe((status: BackgroundGeolocationResponse) => {
            console.log('error=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.authorization).subscribe((status) => {
            console.log('authorization=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.abort_requested).subscribe((status) => {
            console.log('abort_requested=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.http_authorization).subscribe((status) => {
            console.log('http_authorization=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.background).subscribe((status: BackgroundGeolocationResponse) => {
            console.log('background=' + status);
        });

        this.backgroundGeolocation.on(BackgroundGeolocationEvents.foreground).subscribe((status: BackgroundGeolocationResponse) => {
            console.log('background=' + status);

            this.backgroundGeolocation.getLocations().then((locations) => {
                console.log(locations);
                console.log('getLocations=' + locations);
            });
        });

    }

    startTrackingWorkingAndroid(itesm) {

        this.insomnia.keepAwake().then(() => {
            console.log('success insomnia.keepAwake()')
        }, () => {
            console.log('error insomnia.keepAwake()')
        });

        this.network.onDisconnect().subscribe(() => {
            this.isInternetConnected = false;
            console.log('network was disconnected :-(');

            var deviceVersion = '';
            if (this.platform.is('android')) {
                deviceVersion = ANDROID_APP_VERSION;
            } else {
                deviceVersion = IOS_APP_VERSION;
            }

            var messageTemp = 'Int_Disc' + this.constant.dateConvertFormate(new Date(), 'yyyy-MM-dd HH:mm:ss').toString();
            var dic = {
                label: messageTemp,
                version : deviceVersion
            }

            this.sqliteDatabase.saveInternetData(dic);
        });

        this.network.onConnect().subscribe(() => {
            this.isInternetConnected = true;
            console.log('Network Connected');
            this.uploadOfflineLocationAPI();
            this.uploadInternetDatabaseAPI();
        });

        clearInterval(this.intervalTemp);

        this.intervalTemp = setInterval(() => {
            this.updateLocatinoAPI(itesm);
        }, parseInt(this.constant.TimeInterval_Android));

        // this.intervalTemp = setInterval(() => {
        //     this.updateLocatinoAPI(itesm);
        // }, 10000);

    }


    updateLocatinoAPI(jobID) {
        console.log('-----------------Location Get From Device------------------');
        return new Promise((resolve) => {
            this.geolocaction.getCurrentPosition().then((position) => {
                this.actionOnLocationFuc(jobID, position);
                resolve(true);
            }, err => {
                console.log(' Error : ' + JSON.stringify(err));
            });
        });

    }

    actionOnLocationFuc(jobID, resp) {

        if (this.isInternetConnected) {

            var dic = {
                "order_id": jobID.toString(),
                "latitude": resp['coords']['latitude'],
                "longitude": resp['coords']['longitude'],
                "comment": 'Android Not Plugin BG',
            }

            this.service.locationUpdate(dic).subscribe((result) => {
                console.log(result);
            }, (error) => {
                console.log(error.json());
            });

        } else {

            var timeDate = this.constant.dateConvertFormate(new Date(), 'yyyy-MM-dd HH:mm:ss').toString() + ' Offline BG ';
            var dic = {
                "order_id": jobID.toString(),
                "latitude": resp['coords']['latitude'],
                "longitude": resp['coords']['longitude'],
                "comment": timeDate.toString(),
            }

            this.sqliteDatabase.saveLocation(dic);
            this.sqliteDatabase.getData().then(res => {
                console.log('-----------------getData call------------------');
                console.log(res);
            });

        }
    }


    uploadOfflineLocationAPI(){
        console.log('-----------------uploadOfflineLocationAPI call------------------');

        var offlineLocationArry = [];
        this.sqliteDatabase.getData().then(res => {
            console.log(res);
            var tempData:any = res;
            offlineLocationArry = tempData;
            console.log(offlineLocationArry.length);
            if (offlineLocationArry.length != 0) {
                this.service.locationUpdate(offlineLocationArry).subscribe((result) => {
                    console.log(result);
                    this.sqliteDatabase.deleteRecord();
                }, (error) => {
                    console.log(error.json());
                });
            }
        });
    }

    uploadInternetDatabaseAPI(){
        console.log('-----------------uploadInternetDatabaseAPI call------------------');
        var internetDataArry = [];
        this.sqliteDatabase.getDataInternet().then(res => {
            console.log(res);
            var tempData:any = res;
            internetDataArry = tempData;
            console.log(internetDataArry.length);
            if (internetDataArry.length != 0) {
                this.service.StoreAppLog(internetDataArry[0]).subscribe((result) => {
                    console.log(result);
                    this.sqliteDatabase.deleteRecordInternet();
                }, (error) => {
                    console.log(error.json());
                });
            }
        });
    }


}



