
import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()

export class LoadingCustom {

    loading;
    loadingCount = 0;
    constructor(public Loading: LoadingController) {


    }

    LoadingPresent(count) {
        this.loadingCount = this.loadingCount + count;
        if (!this.loading) {

            this.loading = this.Loading.create({});
            
            // this.loading = this.Loading.create({
            //     spinner: 'hide',
            //     content: `<img src="assets/loading/loading.gif" />`,
            // });
            this.loading.present();
        }
    }

    LoadingHide() {

        if (this.loading) {  
            this.loading.dismiss();
            this.loading = null;
        }
    }




}
