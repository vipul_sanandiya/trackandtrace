import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';


// Service
import { Constant } from '../services/constant';
import { LoadingCustom } from '../services/lodingCustom';
import { WebService } from '../services/webService';  
import { WebServiceHandler } from '../services/webServiceHandler';
import { SqliteDatabase } from '../services/sqliteDatabase';


//Helper
import { NoInternetConnectionPage } from '../helper/no-internet-connection/no-internet-connection';
import { ImageViewer } from '../helper/imageViewer/imageViewer';
import { VersionUpdatePage } from '../helper/version-update/version-update';


// Plugin

import { StatusBar } from '@ionic-native/status-bar';  
import { SplashScreen } from '@ionic-native/splash-screen'; 
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Keyboard } from '@ionic-native/keyboard';
import { SignaturePadModule } from 'angular2-signaturepad';
import { CallNumber } from '@ionic-native/call-number';
import { DatePipe } from '@angular/common';
import { OneSignal } from '@ionic-native/onesignal';
import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { Market } from '@ionic-native/market';
import { FlurryAnalytics, FlurryAnalyticsObject, FlurryAnalyticsOptions } from '@ionic-native/flurry-analytics';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Insomnia } from '@ionic-native/insomnia';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

//--------------------------------Login-----------------------------------//
import { LoginPage } from '../pages/login/login/login';
import { RegisterPage } from '../pages/login/register/register';

//--------------------------------Customer-----------------------------------//


import { CustomerTab } from '../pages/customer/tabs/tabs';
import { HomePage } from '../pages/customer/home/home';
import { NotificationPage } from '../pages/customer/notification/notification';
import { ProfilePage } from '../pages/customer/profile/profile';
import { SearchPage } from '../pages/customer/searchPage/searchPage';
import { TrackLoad } from '../pages/customer/trackLoad/trackLoad';

//--------------------------------Driver-----------------------------------//
import { DriverTab } from '../pages/driver/tabs/tabs';
import { LoadDetailPage } from '../pages/driver/loadDetail/loadDetail';
import { MyLoadPage } from '../pages/driver/myLoad/myLoad';
import { PlacePage } from '../pages/driver/place/place';
import { DriverProfile } from '../pages/driver/profile/profile'; 


//--------------------------------Translate-----------------------------------//
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from "@angular/common/http";

import { PlaceCategory } from '../pages/driver/placeCategory/placeCategory';
import { LocationTracker } from '../services/locationTracker'; 
import { ResetPassword } from '../pages/login/resetPassword/resetPassword';
import { TermCondition } from '../pages/login/termCondition/termCondition';
import { AddAddressPage } from '../pages/admin/AddAddress/AddAddress';
import { AddDriverPage } from '../pages/admin/AddDriver/AddDriver';
import { AddLoadFirst } from '../pages/admin/AddLoad/addLoadFirst/addLoadFirst';
import { AddLoadSecond } from '../pages/admin/AddLoad/addLoadSecond/addLoadSecond';
import { AddLoadThird } from '../pages/admin/AddLoad/addLoadThird/addLoadThird';
import { AddressList } from '../pages/admin/addressList/addressList';
import { AddShipperPage } from '../pages/admin/AddShipper/AddShipper';
import { AddUserPage } from '../pages/admin/AddUser/AddUser';
import { DriversListPage } from '../pages/admin/driverslist/DriversList';
import { ShipperListPage } from '../pages/admin/ShipperList/ShipperList';
import { UserListPage } from '../pages/admin/UserList/UserList';
import { LoadList } from '../pages/admin/loadList/loadList';
import { SelectCustomer } from '../pages/admin/selectCustomer/selectCustomer';
import { FlurryManage } from '../services/flurryManage';
import { ImageUpload } from '../helper/imageUpload/imageUpload';
import { EditProfile } from '../pages/admin/editProfile/editProfile';
import { Settings } from '../pages/admin/settings/settings';
import { ChangePassword } from '../pages/admin/changePassword/changePassword';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/translate/', '.json');
}

@NgModule({
  declarations: [  
    MyApp,
    LoginPage,
    RegisterPage,
    ResetPassword,
    TermCondition,

    // Shipper
    CustomerTab,
    HomePage,
    ProfilePage,
    NotificationPage,
    SearchPage,
    TrackLoad,

    // Driver
    DriverTab,
    LoadDetailPage,
    MyLoadPage,
    PlacePage,
    DriverProfile,
    PlaceCategory,

    // Admin
    AddAddressPage,
    AddDriverPage,
    AddLoadFirst,AddLoadSecond, AddLoadThird,

    AddressList,
    AddShipperPage,
    AddUserPage,
    DriversListPage,
    ShipperListPage,
    UserListPage,
    LoadList,
    SelectCustomer,
    EditProfile,
    Settings,
    ChangePassword,
    
    NoInternetConnectionPage,ImageViewer, VersionUpdatePage, ImageUpload
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    SignaturePadModule,
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader, 
        deps: [HttpClient]
      }
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    RegisterPage,
    ResetPassword,
    TermCondition,

    // Shipper
    CustomerTab,
    HomePage,
    ProfilePage,
    NotificationPage,
    SearchPage,
    TrackLoad,

    // Driver
    DriverTab,
    LoadDetailPage,
    MyLoadPage,
    PlacePage,
    DriverProfile,
    PlaceCategory,

    // Admin
    AddAddressPage,
    AddDriverPage,
    AddLoadFirst,AddLoadSecond, AddLoadThird,
    AddressList,
    AddShipperPage,
    AddUserPage,
    DriversListPage,
    ShipperListPage,
    UserListPage,
    LoadList,
    SelectCustomer,
    EditProfile,
    Settings,
    ChangePassword,

    NoInternetConnectionPage,ImageViewer, VersionUpdatePage, ImageUpload
  ],
  providers: [
    StatusBar,
    DatePipe,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Constant, LoadingCustom, WebService, WebServiceHandler, SqliteDatabase,
    Network,
    CallNumber,
    Keyboard,
    OneSignal,
    Geolocation,
    LocationTracker,
    BackgroundGeolocation,
    Market,
    FlurryAnalytics,
    FlurryManage,
    Camera,
    Insomnia,
    SQLite
  ]
})
export class AppModule {}
