import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController, AlertController, App, ViewController, Events, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WebService } from '../services/webService';
import { ANDROID_APP_VERSION, IOS_APP_VERSION, PUSH_SERVER_KEY, PUSH_SENDER_ID } from '../services/config';
import { Keyboard } from '@ionic-native/keyboard';

import { TranslateService } from '@ngx-translate/core';
import { Network } from '@ionic-native/network';

import { Constant } from '../services/constant';
import { NoInternetConnectionPage } from '../helper/no-internet-connection/no-internet-connection';
import { VersionUpdatePage } from '../helper/version-update/version-update';
import { LoginPage } from '../pages/login/login/login';
import { CustomerTab } from '../pages/customer/tabs/tabs';
import { ProfilePage } from '../pages/customer/profile/profile';
import { HomePage } from '../pages/customer/home/home';
import { TrackLoad } from '../pages/customer/trackLoad/trackLoad';
import { DriverTab } from '../pages/driver/tabs/tabs';
import { OneSignal } from '@ionic-native/onesignal';
import { PlacePage } from '../pages/driver/place/place';
import { AddAddressPage } from '../pages/admin/AddAddress/AddAddress';
import { LoadList } from '../pages/admin/loadList/loadList';
import { AddressList } from '../pages/admin/addressList/addressList';
import { ShipperListPage } from '../pages/admin/ShipperList/ShipperList';
import { DriversListPage } from '../pages/admin/driverslist/DriversList';
import { UserListPage } from '../pages/admin/UserList/UserList';
import { AddLoadFirst } from '../pages/admin/AddLoad/addLoadFirst/addLoadFirst';
import { FlurryManage } from '../services/flurryManage';
import { LoadDetailPage } from '../pages/driver/loadDetail/loadDetail';
import { EditProfile } from '../pages/admin/editProfile/editProfile';
import { Settings } from '../pages/admin/settings/settings';
import { AddLoadSecond } from '../pages/admin/AddLoad/addLoadSecond/addLoadSecond';
import { AddLoadThird } from '../pages/admin/AddLoad/addLoadThird/addLoadThird';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SqliteDatabase } from '../services/sqliteDatabase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  backButtonAlertModel: any;
  activeSideMenu = 1;
  userData: any;

  constructor(public menu:MenuController, public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public service: WebService, public modalCtrl: ModalController, public alertCtrl: AlertController,
    public app: App, public network: Network, public constant: Constant, public event: Events, public translate: TranslateService,
    public keyboardCustome: Keyboard, public events: Events, public oneSignal: OneSignal, public flurryMange: FlurryManage, public sqliteDatabase:SqliteDatabase) {

    var self = this;
    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.overlaysWebView(false);

      this.initMethodForApp();

      // back button event handle
      platform.registerBackButtonAction(() => {
        this.HardwareBackButton();
      });

      splashScreen.hide();


    });
  }

  initMethodForApp() {

    var self = this;
    this.events.subscribe('sidemenuDataSet', () => {
      self.setSidemenuData();
    });

    this.menu.swipeEnable(false);

    this.setAppLanguage();
    this.checkAppVersionUpdate();
    this.flurryMange.flurryInit();
    if (this.platform.is('cordova')) {
      this.pushInit();
    }

    this.sqliteDatabase.createTable();
    this.sqliteDatabase.createTableInternet();
  }

  pushInit() {
    var self = this;
    this.oneSignal.startInit(PUSH_SERVER_KEY, PUSH_SENDER_ID);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

    this.oneSignal.handleNotificationReceived().subscribe((data) => {
      console.log('handleNotificationReceived');
      console.log(data);
      console.log(JSON.stringify(data));
    });

    this.oneSignal.handleNotificationOpened().subscribe((data) => {
      console.log(data);
      console.log(JSON.stringify(data));
      console.log('handleNotificationOpened');
      self.UserLoggedINCheck(data.notification.payload);
    });

    this.oneSignal.endInit();

    this.oneSignal.getIds().then((data) => {
      console.log('push token 2');
      console.log(data);
      self.constant.pushToken = data.userId;
    });

  }

  UserLoggedINCheck(data) {
    var accessToken = localStorage.getItem('Token');
    if (accessToken) {
      this.NotificationHandle(data);
    } else {
      console.log('Push notification click but user not login');
    }

  }

  NotificationHandle(notiData) {

    console.log('NotificationHandle', notiData);
    // notification_type = 4 => Tracking off in driver app
    
    if (notiData.additionalData.notification_type == 2 || notiData.additionalData.notification_type == 3) {
      console.log('----------------------Notification Move DetailsPage-----------------------------');
      this.app.getRootNav().push(LoadDetailPage, {orderID : notiData.additionalData.load_id, fromNotification : notiData.additionalData.notification_type});
    } else {
      console.log('diffrent notification');
    }

  }


  setAppLanguage() {

    this.translate.setDefaultLang('en');
    localStorage.setItem('language', 'en');

    // this.translate.setDefaultLang('da');
    // localStorage.setItem('language', 'da');

  }

  checkAppVersionUpdate() {
    this.service.getAppVersion().subscribe(data => {
      console.log(data);
      if (data.status) {
        var responceAPI = data['data'];
        this.constant.TimeInterval_IOS = responceAPI[2].time_interval;
        this.constant.TimeInterval_Android = responceAPI[1].time_interval;
        if (this.platform.is('ios')) {
          if (IOS_APP_VERSION < responceAPI[2].version) {
            if (responceAPI[2].force_update) {
              this.app.getRootNav().push(VersionUpdatePage, { data: responceAPI[2] });
            } else {
              this.CheckLoginSession();
            }
          } else {
            this.CheckLoginSession();
          }
        } else {
          if (ANDROID_APP_VERSION < responceAPI[1].version) {
            if (responceAPI[1].force_update) {
              this.app.getRootNav().push(VersionUpdatePage, { data: responceAPI[1] });
            } else {
              this.CheckLoginSession();
            }
          } else {
            this.CheckLoginSession();
          }
        }

      } else {
        this.CheckLoginSession();
      }

    }, err => {
      this.CheckLoginSession();
      console.log('checkAppVersion API Issue', err)
    });
  }

  //---------------------------------------Login Session---------------------------------//

  CheckLoginSession()  {
    
    if (localStorage.getItem("userData") !== null) {
      
      if (localStorage.getItem("role") !== null || localStorage.getItem("role") != undefined) {
        if (localStorage.getItem("role") == '1' || localStorage.getItem("role") == '4') {
          this.setSidemenuData();
          this.rootPage = LoadList;
        }
        if (localStorage.getItem("role") == '2') {
          this.storeAppLogAPI();
          this.rootPage = DriverTab;
        }
        if (localStorage.getItem("role") == '3') {
          this.rootPage = CustomerTab;
        }

      } else {
        this.rootPage = PlacePage;
      }

    } else {
      console.log('CheckLoginSession 5');
      this.rootPage = LoginPage;
    }
  }

  storeAppLogAPI() {
    var versionApp = ANDROID_APP_VERSION;
    if (this.platform.is('ios')) {
       versionApp = IOS_APP_VERSION;
    }
    var dic = {
      label : 'app_start',
      version : versionApp
    }
    this.service.StoreAppLog(dic).subscribe(data => {
      console.log(data);
    }, err => {
      console.log('storeAppLogAPI Issue', err)
    });
  }

  setSidemenuData() {
    this.activeSideMenu = 1;
    this.userData = this.constant.getUserData();
    console.log(this.userData);
  }

  //---------------------------------------Hardware Back Button---------------------------------//

  HardwareBackButton() {
    let nav = this.app.getActiveNav();
    console.log('HardwareBackButton=>1');
    console.log(nav);
    let activeView: ViewController = nav.getActive();
    console.log('HardwareBackButton=>2');
    console.log(activeView);
    if (activeView != null) {
      if (nav.canGoBack()) {
        const overlayView = this.app._appRoot._overlayPortal._views[0];
        console.log('HardwareBackButton=>3');
        console.log(overlayView);
        if (overlayView && overlayView.dismiss) {
          overlayView.dismiss();
        } else {
          nav.pop();
        }
      } else {
        if (!this.backButtonAlertModel) {
          this.backButtonAlert();
        }
      }
    }
  }

  backButtonAlert() {
    this.backButtonAlertModel = this.alertCtrl.create({
      title: 'Exit?',
      message: 'Do you want to exit the app?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.backButtonAlertModel = null;
          }
        },
        {
          text: 'Exit',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.backButtonAlertModel.present();
  }

  //---------------------------------------Internet Connection---------------------------------//

  CheckInternetConnection() {
    var self = this;

    this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      if (!self.constant.isNetworkAvailablePage) {
        let profileModal = self.modalCtrl.create(NoInternetConnectionPage);
        self.constant.isNetworkAvailablePage = true;
        profileModal.present();
      }

    });

    if (this.platform.is('cordova')) {
      if (this.network.type === 'wifi' || this.network.type === 'ethernet' || this.network.type === '4g' || this.network.type === 'cellular') {
        console.log("Network error");
      } else {
        if (!self.constant.isNetworkAvailablePage) {
          let profileModal = self.modalCtrl.create(NoInternetConnectionPage);
          self.constant.isNetworkAvailablePage = true;
          profileModal.present();
        }
      }
    } else {
      console.log('cordova not available');
    }

  }

  sideMenuClick(indexClick) {
    var actualIndex = indexClick - 1;
    this.event.publish('sideMenuClick', { data: actualIndex });
  }

  logOutClick() {
    this.rootPage = LoginPage;
  }

  pageOpenFromSidemenu(selectedPage) {
    if (selectedPage == 1) {
      this.nav.setRoot(LoadList);

    } else if (selectedPage == 2) {
      this.nav.setRoot(AddressList);
    } else if (selectedPage == 3) {
      this.nav.setRoot(ShipperListPage);
    } else if (selectedPage == 4) {
      this.nav.setRoot(DriversListPage);
    } else if (selectedPage == 5) {
      this.nav.setRoot(UserListPage);
    } else if (selectedPage == 6) {
      this.nav.setRoot(Settings);
    } else if (selectedPage == 7) {
      localStorage.clear();
      this.nav.setRoot(LoginPage);
    }


  }

}
