#!/usr/bin/env node

var fs = require('fs');
var path = require('path');

var filestocopy = [{
    "hooks/notificationIcon/android/drawable-hdpi/ic_stat_onesignal_default.png":
        "platforms/android/app/src/main/res/drawable-port-hdpi/ic_stat_onesignal_default.png"
}, {
    "hooks/notificationIcon/android/drawable-mdpi/ic_stat_onesignal_default.png":
        "platforms/android/app/src/main/res/drawable-port-mdpi/ic_stat_onesignal_default.png"
}, {
    "hooks/notificationIcon/android/drawable-xhdpi/ic_stat_onesignal_default.png":
        "platforms/android/app/src/main/res/drawable-port-xhdpi/ic_stat_onesignal_default.png"
}, {
    "hooks/notificationIcon/android/drawable-xxhdpi/ic_stat_onesignal_default.png":
        "platforms/android/app/src/main/res/drawable-port-xxhdpi/ic_stat_onesignal_default.png"
}, {
    "hooks/notificationIcon/android/drawable-xxxhdpi/ic_stat_onesignal_default.png":
        "platforms/android/app/src/main/res/drawable-port-xxxhdpi/ic_stat_onesignal_default.png"
}];



module.exports = function (context) {

    // no need to configure below
    var rootdir = context.opts.projectRoot;
    console.log("ROOT_DIR", rootdir);
    var notificationToneDir = rootdir + "/platforms/android/app/src/main/res/raw";

    try {
        fs.mkdir(notificationToneDir, { recursive: true }, (err) => {
            console.log("Notification sound file folder creating", err);
        });
    } catch (e) {
        console.log(e);
        console.log("Notification sound file folder not created.");
    }

    filestocopy.forEach(function (obj) {
        Object.keys(obj).forEach(function (key) {
            var val = obj[key];
            var srcfile = path.join(rootdir, key);
            var destfile = path.join(rootdir, val);
            console.log("copying " + srcfile + " to " + destfile);
            var destdir = path.dirname(destfile);
            if (fs.existsSync(srcfile) && fs.existsSync(destdir)) {
                fs.createReadStream(srcfile).pipe(
                    fs.createWriteStream(destfile));
            }
        });
    });

};